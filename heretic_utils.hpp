/*
    ___           __      _      __ __            __  _
   / _ \_______  / /____ (_)__  / // /__ _______ / /_(_)___
  / ___/ __/ _ \/ __/ -_) / _ \/ _  / -_) __/ -_) __/ / __/
 /_/  /_/  \___/\__/\__/_/_//_/_//_/\__/_/  \__/\__/_/\__/
                                                           
 
*/

#ifndef PRO_HERETIC_UTILS_HPP
#define PRO_HERETIC_UTILS_HPP
#pragma once

#include <assert.h>

#include <iostream>
#include <sstream>
#include <string>
#include <vector>

using std::cout;
using std::endl;
using std::ostream;
using std::string;
using std::stringstream;
using std::vector;

//////////////////////////////////////////////////////////////////////////////////////////////
namespace ProteinHeretic{
    class Node {
       public:
        string sequence;
        int index, matrix_location;

        inline Node(){};
        Node(int index): 
            index(index) 
            {}

        Node(string &sequence): 
            sequence(sequence)
            {}
        
        Node(int index, string &sequence): 
            index(index), sequence(sequence) 
            {}
    };

    class PeptideNode : public Node {
       public:
        vector<string> matched_proteins;
        vector<float> nsp_values;
        bool degenerate = false;
        int num_matched_proteins = 0, num_instances = 0;
        float init_probability = 0, probability = 0, prev_probability = 0;
        float fval = 0.00, massdiff = 0.00;
        int charge = 0, ntt = 2, nmc = 0;
        float calc_neutral_pep_mass = 0.0;

        // General mod info
        bool has_mod_info = false;
        string modified_peptide = "";
        vector<modification_info> mod_info;
        
        // nterm and cterm mod info
        bool has_nterm_mod = false;
        bool has_cterm_mod = false;
        float nterm_mass = 0.0;
        float cterm_mass = 0.0;

        inline PeptideNode(): 
            Node()
            {}
        
        inline PeptideNode(int id, string &sequence, float &probability): 
            Node(id, sequence), 
            probability(probability)
            {}

        inline PeptideNode(int index, string &sequence, float probability, int num_matched_proteins): 
            Node(index, sequence), 
            probability(probability),
            num_matched_proteins(num_matched_proteins)
            {}
    };

    class ProteinNode : public Node {
       public:
        string name;
        float weight = 0, rate_of_change = 0;
        float probability = 1, prev_probability = 0;
        int num_matched_peptides = 0;
        vector<int> connected_peptides;
        vector<float> peptide_scores;
        float confidence = 0.0;

        inline ProteinNode(): 
            Node()
            {}

        inline ProteinNode(int index, string &name, int num_matched_peps): 
            Node(index), name(name), 
            num_matched_peptides(num_matched_peps)
            {}

        inline ProteinNode(int index, string &sequence, string &name, int num_matched_peps): 
            Node(index, sequence), 
            name(name), 
            num_matched_peptides(num_matched_peps)
            {}
    };
}  // namespace ProteinHeretic

/*
    We can add an edge class here
        each edge contains a left node index and a right node index
        also contains a weight property

        this effectively draws a connection between two nodes and the weight associated with it
        Then, each node will contain a vector of edge index's and the graph itself will contain a 
        vector of type edges
*/
//////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////
namespace ProteinHeretic {
    template <class T>
    class Matrix {
       public:
        vector<vector<T> > elements;
        int rows;
        int columns;

        // Constructors with initializer lists
        Matrix() : rows(0), columns(0) {}
        Matrix(int rows, int columns) : rows(rows), columns(columns), elements(vector<vector<T> >(rows, vector<T>(columns))) {}
        Matrix(vector<vector<T> > const &elements) : rows(elements.size()), columns(elements[0].size()), elements(elements) {}

        // Functions
        Matrix<T> add(Matrix<T> const &matrix) const;
        Matrix<T> subtract(Matrix<T> const &matrix) const;
        Matrix<T> dot(Matrix<T> const &matrix) const;
        Matrix<T> transpose() const;
        Matrix<T> apply_func(T (*function)(T)) const;
        Matrix<T> multiply(T const &value);
        Matrix<T> multiply(Matrix<T> const &matrix) const;
        vector<T> get_row_vals(int row);
        vector<T> get_col_vals(int col);

        void print() const;
    };
    /*----------------------------------
        Performs matrix addition
    ------------------------------------*/
    template <typename T>
    inline Matrix<T> Matrix<T>::add(Matrix<T> const &matrix) const {
        // Raise an exception if the dimensions of the matrices are misaligned
        assert(rows == matrix.rows && columns == matrix.columns);

        Matrix<T> result_matrix(rows, columns);

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                result_matrix.elements[i][j] = elements[i][j] + matrix.elements[i][j];
            }
        }
        return (result_matrix);
    }
    /*-----------------------------------
        Performs matrix subtraction
    ------------------------------------*/
    template <typename T>
    inline Matrix<T> Matrix<T>::subtract(Matrix<T> const &matrix) const {
        // Raise an exception if the dimensions of the matrices are misaligned
        assert(rows == matrix.rows && columns == matrix.columns);

        Matrix<T> result_matrix(rows, columns);

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                result_matrix.elements[i][j] = elements[i][j] - matrix.elements[i][j];
            }
        }
        return (result_matrix);
    }
    /*--------------------------------------------
      Performs the dot product of two matrices
    ---------------------------------------------*/
    template <typename T>
    inline Matrix<T> Matrix<T>::dot(Matrix<T> const &matrix) const {
        // In order to take the dot product, matrices must be aligned such that
        // the columns of Matrix 1 are the same as the rows in Matrix 2
        assert(columns == matrix.rows);

        int matrix_columns = matrix.columns;
        double product = 0;

        Matrix<T> result_matrix(rows, matrix_columns);

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < matrix_columns; j++) {
                for (int h = 0; h < columns; h++) {
                    product += elements[i][h] * matrix.elements[h][j];
                }
                result_matrix.elements[i][j] = product;
                product = 0;
            }
        }
        return (result_matrix);
    }
    /*------------------------------------------------------
      Transposes a matrix i.e. element i,j becomes j,i
    -------------------------------------------------------*/
    template <typename T>
    inline Matrix<T> Matrix<T>::transpose() const {
        Matrix<T> result_matrix(columns, rows);

        for (int i = 0; i < columns; i++) {
            for (int j = 0; j < rows; j++) {
                result_matrix.elements[i][j] = elements[j][i];
            }
        }
        return (result_matrix);
    }
    /*---------------------------------------------------
      Applies a function to each element in the matrix
    ----------------------------------------------------*/
    template <typename T>
    inline Matrix<T> Matrix<T>::apply_func(T (*function)(T)) const {
        Matrix<T> result_matrix(rows, columns);

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                result_matrix.elements[i][j] = (*function)(elements[i][j]);
            }
        }
        return (result_matrix);
    }
    /*---------------------------------------------------
      Performs element-wise matrix multiplication
    ----------------------------------------------------*/
    template <typename T>
    inline Matrix<T> Matrix<T>::multiply(T const &value) {
        Matrix<T> result_matrix(rows, columns);

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                result_matrix.elements[i][j] = elements[i][j] * value;
            }
        }
        return (result_matrix);
    }
    /*----------------------------------------------
      Performs matrix to matrix multiplication
    ------------------------------------------------*/
    template <typename T>
    inline Matrix<T> Matrix<T>::multiply(Matrix<T> const &matrix) const {
        // Raise an exception if dimensions of matrices are misaligned
        assert(this->rows == matrix.rows && this->columns == matrix.columns);

        Matrix<T> result_matrix(rows, columns);

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < this->columns; j++) {
                result_matrix.elements[i][j] = this->elements[i][j] * matrix.elements[i][j];
            }
        }
        return (result_matrix);
    }
    /*-----------------------------------------------------
      Returns all of the values in the given row.
    ------------------------------------------------------*/
    template <typename T>
    inline vector<T> Matrix<T>::get_row_vals(int row) {
        vector<T> values;

        for (int i = 0; i < columns; i++) {
            values.push_back(elements[row][i]);
        }
        return (values);
    }
    /*--------------------------------------------------------
      Returns all of the values in the given column
    ----------------------------------------------------------*/
    template <typename T>
    inline vector<T> Matrix<T>::get_col_vals(int col) {
        vector<T> values;

        for (int i = 0; i < rows; i++) {
            if (elements[i][col] != 0) {
                values.push_back(elements[i][col]);
            }
        }
        return (values);
    }
    /*---------------------------------------------------------
      Prettier way of printing elements of a matrix.
    ----------------------------------------------------------*/
    template <typename T>
    inline void Matrix<T>::print() const {
        for (int i = 0; i < rows; ++i) {
            cout << "[";
            for (int j = 0; j < columns; ++j) {
                cout << " " << elements[i][j] << " ";
            }
            cout << "]" << endl;
        }
    }
}  // namespace ProteinHeretic
//////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////
namespace ProteinHeretic {
    template <class T1, class T2>
    class Graph {
       public:
        vector<T1> left_nodes;
        vector<T2> right_nodes;
        Matrix<float> edges;
        int num_left_nodes;
        int num_right_nodes;

        inline Graph(){};
        Graph(int num_left_nodes, int num_right_nodes);
        Graph(vector<T1> left_nodes, vector<T2> right_nodes) : left_nodes(left_nodes), right_nodes(right_nodes) {}
    };
}  // namespace ProteinHeretic

//////////////////////////////////////////////////////////////////////////////////////////////
namespace ProteinHeretic {
    template <typename T>
    struct bin {
        int id;
        T min;
        T max;
        vector<int> member_ids;
        T pos_distribution;
        T neg_distribution;
        int total_assignments = 0;
        inline bin(int id, T min, T max) : id(id), min(min), max(max){};
    };
}  // namespace ProteinHeretic
//////////////////////////////////////////////////////////////////////////////////////////////

class HereticError{
    public:
        std::string error_message;
        HereticError(){
            this->default_msg();
            this->dev_msg();
        }

        explicit
        HereticError(const std::string msg):
            error_message(msg)
            {this->print_msg();}
        
        void default_msg(){
            std::cout << "[HERETIC ERROR]: Whoops, looks like we ran into some unforseen behavior" << std::endl;
            std::cout << "... ProteinHeretic could not finish..." << std::endl;
        }

        void print_msg(){
            std::cout << "[HERETIC ERROR]: " << this->error_message << endl;
        }

        void dev_msg(){
            std::cout << "If this continues to give you trouble,";
            std::cout << " you can submit an issue at: \n https://gitlab.com/jackcampanella/proteinheretic" << endl;
            std::cout << "with your input files/all other relevant information." << endl;
    }
};

class HereticBanner{
    public:
        HereticBanner(){
            this->show_banner();
        }
        void show_banner(){
            std::cout << "\t\t| Protein Heretic | " << std::endl;
            std::cout << "(Inspiration from ProteinProphet by Keller et. al.)\n" << std::endl;
        }      
};

#endif

/*************************************************************
        * PepXml Parser Library *
    * Utilizes rapidxml's xml parser to parse pepxml files
      particular to those that are output from MS/MS database
      search algorithm results. 

****************************************************************/

#pragma once
#ifndef PEP_XML_HPP
#define PEP_XML_HPP

#include <math.h>

#include <cassert>
#include <iostream>
#include <vector>

#include "rapidxml-1.13/rapidxml.hpp"
#include "rapidxml-1.13/rapidxml_utils.hpp"
#include "search_data.hpp"

using std::cout;
using std::endl;
using std::string;
using std::vector;

using namespace rapidxml;

class PepXml {
   public:
    static vector<search_data> parse(string &file);
    static string get_search_method(string &file);
    static string get_sample_enzyme(string &file);

   protected:
    static vector<search_data> parse_pepxml(rapidxml::xml_document<> &doc);
    static search_data build_assignment(rapidxml::xml_node<> *node, int charge, string &spectrum_id);
    static unordered_map<string, float> get_search_scores(rapidxml::xml_node<> *node);
};

#endif

#define PEPXML_PARSE_ERROR(what, where) throw parse_error(what, where)

namespace pepxml_parser {
    class parse_error : public std::exception {
       public:
        // Constructs error
        parse_error(const char *what, void *where) : err_what(what), err_where(where) {}

        // Readable description of error
        virtual const char *what() const throw() {
            return err_what;
        }

        // Gets pointer to character data where error happened
        template <class Ch>
        Ch *where() const {
            return reinterpret_cast<Ch *>(err_where);
        }

       private:
        const char *err_what;
        void *err_where;
    };
}  // namespace pepxml_parser


/*-------------------------------------------------------------
    Coordinates the parsing of the pepxml document
--------------------------------------------------------------*/
inline vector<search_data> PepXml::parse(string &file) {
    vector<search_data> results;
    string search_method;

    rapidxml::file<> xmlFile(file.c_str());
    rapidxml::xml_document<> doc;
    doc.parse<0>(xmlFile.data());

    xml_node<> *root_node = doc.first_node();
    xml_node<> *search_summary_node = root_node->first_node("msms_run_summary")->first_node("search_summary");

    // Retaining search method
    search_method = std::string(search_summary_node->first_attribute("search_engine")->value());
    cout << "Detected Search Engine: " << search_method << endl;
    
    // Parsing the actual pepxml doc
    results = PepXml::parse_pepxml(doc);

    return (results);
}

/*-------------------------------------------------------------
    Returns the search engine indicated in the pepxml file
--------------------------------------------------------------*/
inline string PepXml::get_search_method(string &file) {
    string search_method;

    rapidxml::file<> xmlFile(file.c_str());
    rapidxml::xml_document<> doc;
    doc.parse<0>(xmlFile.data());

    xml_node<> *root_node = doc.first_node();
    xml_node<> *search_summary_node = root_node->first_node("msms_run_summary")->first_node("search_summary");

    search_method = std::string(search_summary_node->first_attribute("search_engine")->value());

    return (search_method);
}

/*--------------------------------------------------------
    Returns the Sample Enzyme specified in the Pepxml
---------------------------------------------------------*/
inline string PepXml::get_sample_enzyme(string &file){
    string sample_enzyme = "";

    rapidxml::file<> xmlFile(file.c_str());
    rapidxml::xml_document<> doc;
    doc.parse<0>(xmlFile.data());

    // Sample enzyme node is a child of msms_run_summary
    xml_node<> *root_node = doc.first_node();
    xml_node<> *sample_enzyme_node = root_node->first_node("msms_run_summary")->first_node("sample_enzyme");

    // Retaining name attribute
    sample_enzyme = std::string(sample_enzyme_node->first_attribute("name")->value());

    return(sample_enzyme);
}

/*-------------------------------------------------------------------
    Returns the search scores associated with a search_hit element
--------------------------------------------------------------------*/
inline unordered_map<string, float> PepXml::get_search_scores(rapidxml::xml_node<> *node){
    unordered_map<string, float> scores;
    
    // Processing over all search scores associated with this hit
    xml_node<> *score = node->first_node("search_score");
    while (score != NULL) {
        // Storing score's value (accounting for nan's)
        if (!isnan(atof(score->first_attribute("value")->value()))) {
            auto score_name = score->first_attribute("name")->value();
            scores[score_name] = atof(score->first_attribute("value")->value());
            
        } else {
            scores[score->first_attribute("name")->value()] = 0.0;
        }

        // Moving to next score
        score = score->next_sibling("search_score");
    }
    
    return(scores);
}

/*-----------------------------------------------------------------------------------
    Parses and returns all of the information associated with the peptide assignment
------------------------------------------------------------------------------------*/
inline search_data PepXml::build_assignment(rapidxml::xml_node<> *node, int charge, string &spectrum_id){
    search_data assignment;
    
    assignment.peptide_sequence = node->first_attribute("peptide")->value();
    
    // Retaining protein match information
    assignment.matched_protein = node->first_attribute("protein")->value();
    assignment.num_matched_proteins = atoi(node->first_attribute("num_tot_proteins")->value());
    
    // Storing spectra's auxillary info: ions, charge, ntt, etc.
    assignment.num_matched_ions = atoi(node->first_attribute("num_matched_ions")->value());
    assignment.total_num_ions = atoi(node->first_attribute("tot_num_ions")->value());
    assignment.charge = charge;
    assignment.calc_neutral_pep_mass = atof(node->first_attribute("calc_neutral_pep_mass")->value());
    assignment.spectrum_id = spectrum_id;

    if(node->first_node("modification_info")){
        xml_node<> *mod_info_node = node->first_node("modification_info");
        while(mod_info_node != NULL){
            assignment.has_mod_info = true;
            
            // Checks if modified_peptide attribute exists
            if(mod_info_node->first_attribute("modified_peptide")){
                assignment.modified_peptide = mod_info_node->first_attribute("modified_peptide")->value();
            }

            // Checks if mod_nterm_mass attribute exists
            if(mod_info_node->first_attribute("mod_nterm_mass")){
                assignment.has_nterm_mod = true;
                assignment.nterm_mass = atof(mod_info_node->first_attribute("mod_nterm_mass")->value());
            }

            // Checks if mod_cterm_mass attribute exists
            if(mod_info_node->first_attribute("mod_cterm_mass")){
                assignment.has_cterm_mod = true;
                assignment.cterm_mass = atof(mod_info_node->first_attribute("mod_cterm_mass")->value());
            }

            if(mod_info_node->first_node("mod_aminoacid_mass")){
                
                xml_node<> *mod_aminoacid_mass = mod_info_node->first_node("mod_aminoacid_mass");
                while(mod_aminoacid_mass != NULL){
                    modification_info curr_mod_info;
                    curr_mod_info.position = atoi(mod_aminoacid_mass->first_attribute("position")->value());
                    curr_mod_info.mass = atof(mod_aminoacid_mass->first_attribute("mass")->value());

                    assignment.mod_info.push_back(curr_mod_info);
                    mod_aminoacid_mass = mod_aminoacid_mass->next_sibling("mod_aminoacid_mass");
                }
            }
            mod_info_node = mod_info_node->next_sibling("modification_info");
        }        
    }
    
    // Checks to see if NTT is a valid attribute of the element
    if(node->first_attribute("num_tol_term")){
      assignment.num_termini = atoi(node->first_attribute("num_tol_term")->value());
    }else{
        assignment.num_termini = 2;
    }
    
    // Checks if NMC is a valid attribute of the element
    if(node->first_attribute("num_missed_cleavages")){
        assignment.num_missed_cleavs = atoi(node->first_attribute("num_missed_cleavages")->value());
    }else{
        assignment.num_missed_cleavs = 0;
    }
    
    // TODO: need to have a catch for weird looking mass values with +-
    assignment.mass_diff = atof(node->first_attribute("massdiff")->value());
    
    // Retaining the search scores associated with this peptide
    assignment.scores = PepXml::get_search_scores(node);
    
    return(assignment);
}

/*----------------------------------------------------------------------------------
    Handles the traversing of the pepxml document and retaining peptide information
-----------------------------------------------------------------------------------*/
inline vector<search_data> PepXml::parse_pepxml(rapidxml::xml_document<> &doc){
        vector<search_data> results;

        std::cout << "...Extracting search results" << std::endl;

        // Begin parsing at the first 'spectrum query' node
        xml_node<> *query_node = doc.first_node()->first_node("msms_run_summary")->first_node("spectrum_query");
        
        // Processing over all 'spectrum query' nodes
        while (query_node != NULL) {
            xml_node<> *search_hit = query_node->first_node("search_result")->first_node("search_hit");
            auto charge = atoi(query_node->first_attribute("assumed_charge")->value());
            string spectrum_id = query_node->first_attribute("spectrum")->value();
            
            // Processing over all search hits
            while (search_hit != NULL) {
                
                // We are only interested in search hits of rank 1
                if(atoi(search_hit->first_attribute("hit_rank")->value()) == 1){
                    string prot = search_hit->first_attribute("protein")->value();
                    
                    // Only interested in peptides that aren't associated with decoys
                    if(prot.find("DECOY") == std::string::npos){
                      search_data current_hit = PepXml::build_assignment(search_hit, charge, spectrum_id);
                      current_hit.scores["mass_diff"] = current_hit.mass_diff;
                      results.push_back(current_hit);
                    }
                }
                // Moving to next search hit node
                search_hit = search_hit->next_sibling("search_hit");
            }
            
            // Moving to next spectrum query node
            query_node = query_node->next_sibling("spectrum_query");
        }
        cout << "...Identified " << results.size() << " spectra assignments" << endl;
        return (results);
}


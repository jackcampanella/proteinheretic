/*
  
   ___           __  _    __    __ __            __  _    
  / _ \___ ___  / /_(_)__/ /__ / // /__ _______ / /_(_)___
 / ___/ -_) _ \/ __/ / _  / -_) _  / -_) __/ -_) __/ / __/
/_/   \__/ .__/\__/_/\_,_/\__/_//_/\__/_/  \__/\__/_/\__/ 
        /_/                                               
 
*/

#pragma once
#ifndef SEARCH_DATA_HPP
#define SEARCH_DATA_HPP

#include <iostream>
#include <unordered_map>
#include <vector>

using std::string;
using std::unordered_map;
using std::vector;

struct modification_info {
    int position = 0;
    float mass = 0.0;
};

struct search_data {
    string peptide_sequence;
    int id = 0;
    string spectrum_id = "";
    string matched_protein;
    int num_matched_proteins = 0;
    
    int charge = 0, num_termini = 2, num_missed_cleavs = 0;
    int num_matched_ions = 0, total_num_ions = 0;
    float mass_diff = 0.00;
    float calc_neutral_pep_mass = 0.0;

    // PSM peptide modification info
    bool has_mod_info = false;
    string modified_peptide = "";
    vector<modification_info> mod_info;

    // Mod info for nterm and cterm 
    bool has_nterm_mod = false;
    bool has_cterm_mod = false;
    float nterm_mass = 0.0;
    float cterm_mass = 0.0;

    // Map of search engine search scores
    unordered_map<string, float> scores;
};

#endif

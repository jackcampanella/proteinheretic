/*
  
   ___           __  _    __    __ __            __  _    
  / _ \___ ___  / /_(_)__/ /__ / // /__ _______ / /_(_)___
 / ___/ -_) _ \/ __/ / _  / -_) _  / -_) __/ -_) __/ / __/
/_/   \__/ .__/\__/_/\_,_/\__/_//_/\__/_/  \__/\__/_/\__/ 
        /_/                                               
 
*/

#ifndef DFA_UTILS_HPP
#define DFA_UTILS_HPP

#include "coefficients.hpp"
#include "heretic_utils.hpp"
#include "discriminant_functions.hpp"

/*********************************************************************************
 * Function: xcorr_pime()
 * Transforms xcorr scores into xcorr prime. See documentation/research 
 * publication for details on this function.
**********************************************************************************/
static float xcorr_prime(float &xcorr_score, int &peptide_length, int &num_ions, int &charge) {
    float xcorr_prime = 0.0;
    
    //TODO: num ions not used?
    float L = 0.0;
    float MAX_PEP_LEN = 0.0;
    float num_fragment_ions = 0.0;
    
    if(charge == 1){
        MAX_PEP_LEN = 100.0;
        num_fragment_ions = 2;
    } else if (charge == 2){
        MAX_PEP_LEN = 15.0;
        num_fragment_ions = 2;
    }
    // Charge >= 3
    else{
        MAX_PEP_LEN = 25.0;
        num_fragment_ions = 4;
    }
    
    L = peptide_length;
    if(L > MAX_PEP_LEN){
        L = MAX_PEP_LEN;
    }
    assert(!isnan(L) && !isnan(num_fragment_ions) && !isnan(xcorr_score));
    assert(!isinf(L) && !isinf(num_fragment_ions) && !isinf(xcorr_score));
    // if(isinf(log(xcorr_score))){
    //     cout << "Taking the log of this xcorr score results in an infinity value" << endl;
    //     cout << "Heres the xcorr score: " << xcorr_score << endl;
    // }
    // assert(!isinf(log(xcorr_score)));
    
    // Nan values are returned with log when values too close to 0 are passed in
    (isnan(log(xcorr_score))) ? xcorr_prime = 0.0 : xcorr_prime = (log(xcorr_score)) / log(L * num_fragment_ions);

    // -inf values are returned when values equal to 0 are passed to log
    (isinf(log(xcorr_score))) ? xcorr_prime = 0.0 : xcorr_prime = (log(xcorr_score)) / log(L * num_fragment_ions);
    
    assert(!isnan(xcorr_prime));
    assert(!isinf(xcorr_prime));
    
    return (xcorr_prime);
}

/*********************************************************************************************
 * Function: transform_scores()
 * Facilitates the transformation of xcorr and sprank scores. PeptideProphet's 
 * publication explains that this particular transformation of these scores leads
 * to better discrimination between correct and incorrect assignments
************************************************************************************************/
void transform_scores(spectra &spectra) {
    // Checking if this spectra has an xcorr score
    if (spectra.scores.find("xcorr") != spectra.scores.end()) {
        // xcorr becomes xcorr' (see documentation)
        int pep_length = (int)spectra.sequence.size();
//        spectra.scores["xcorr"] = xcorr_prime(spectra.scores["xcorr"], pep_length, spectra.total_num_ions, spectra.charge);
        spectra.scores["xcorr"] = xcorr_prime(spectra.scores["xcorr"], pep_length, spectra.num_matched_ions, spectra.charge);
    }

    // Checking for sprank score
    if (spectra.scores.find("sprank") != spectra.scores.end()) {
        // Sprank becomes ln(sprank)
        spectra.scores["sprank"] = log(spectra.scores["sprank"]);
        
    }
}

/*****************************************************************************************************
 * Function: discriminant_function()
 * Uses the coefficients of the respective charge group to build discriminant functions 
 * based off of search scores. Then uses this discriminant function to determine a 
 * 'Discriminant Score' for each assignment
*********************************************************************************************************/
void discriminant_function(string &search_method, vector<spectra> &all_spectra) {

    for (auto &spectra : all_spectra){
        if (search_method == "SEQUEST") {
            float xcorr = spectra.get_score("xcorr");
            float delta = spectra.get_score("deltacn");
            float sprank = spectra.get_score("sprank");
            float massd = spectra.mass_diff;
            int sequence_length = spectra.sequence.size();

            spectra.discriminant_score = Discriminant_Functions::sequest(spectra.charge, xcorr, delta, sprank, massd, sequence_length);
        
        } else if (search_method == "X! Tandem") {
            float hyper_score = spectra.get_score("hyperscore");
            float e_score = spectra.get_score("expect");
            float next_score = spectra.get_score("nextscore");
            float sequence_length = spectra.sequence.size();

            spectra.discriminant_score = Discriminant_Functions::tandem(spectra.charge, hyper_score, e_score, next_score, sequence_length);
        
        } else if (search_method == "Comet") {
            float xcorr = spectra.get_score("xcorr");
            float delta = spectra.get_score("deltacn");
            float sprank = spectra.get_score("sprank");
            float massd = spectra.mass_diff;
            int sequence_length = spectra.sequence.size();
            spectra.discriminant_score = Discriminant_Functions::comet(spectra.charge, xcorr, delta, sprank, massd, sequence_length);

        } else if(search_method == "MyriMatch"){
            spectra.discriminant_score = Discriminant_Functions::myrimatch(spectra.charge);
        } else if(search_method == "Morpheus"){
            float morpheus_score = spectra.get_score("Morpheus Score");
            int sequence_length = spectra.sequence.size();

            spectra.discriminant_score = Discriminant_Functions::morpheus(spectra.charge, morpheus_score, sequence_length);
        }
        else {
            PeptideHeretic::HereticWarning("Detected un-supported search engine. Using naive approach to derive Discriminant Scores");
            //TODO: implement naive approach for d scores
            assert(false);
            
        }
    }

    
//     unordered_map<string, float> single_coeffs;
//     unordered_map<string, float> double_coeffs;
//     unordered_map<string, float> triple_coeffs;
//     if (search_method == "SEQUEST") {
//         single_coeffs = single_charged_ions::sequest;
//         double_coeffs = double_charged_ions::sequest;
//         triple_coeffs = triple_charged_ions::sequest;
//     } else if (search_method == "X! Tandem") {
//         single_coeffs = single_charged_ions::tandem;
//         double_coeffs = double_charged_ions::tandem;
//         triple_coeffs = triple_charged_ions::tandem;
//     } else if (search_method == "Comet") {
//         single_coeffs = single_charged_ions::comet;
//         double_coeffs = double_charged_ions::comet;
//         triple_coeffs = triple_charged_ions::comet;
//     } else if(search_method == "MyriMatch"){
//         single_coeffs = single_charged_ions::myrimatch;
//         double_coeffs = double_charged_ions::myrimatch;
//         triple_coeffs = triple_charged_ions::myrimatch;
//     } else if(search_method == "Morpheus"){
//         single_coeffs = single_charged_ions::morpheus;
//         double_coeffs = double_charged_ions::morpheus;
//         triple_coeffs = triple_charged_ions::morpheus;
//     }
//     else {
//         PeptideHeretic::HereticWarning("Detected un-supported search engine. Using naive approach to derive Discriminant Scores");
//         //TODO: implement naive approach for d scores
//         assert(false);
        
//     }

//     // Performing discriminant function on each search result
//     for (auto &spectra : all_spectra) {
//         long double d_score = 0.0;
//         int charge = spectra.charge;
//         double current_mass_diff = abs(spectra.mass_diff);
//         assert(current_mass_diff >= 0);

//         // Iterating through all of the spectra's scores
//         for (auto score : spectra.scores) {
//             switch (charge) {
//                 // Spectra of charge 1
//                 case (1): {
//                     d_score += score.second * single_coeffs[score.first];
//                     assert(!isnan(score.second));
//                     assert(!isnan(single_coeffs[score.first]));

//                     if(isinf(score.second)){
//                         cout << "Detected infinity values" << endl;
//                         cout << "Score.first: " << score.first << endl;
//                         cout << "Score.second: " << score.second << endl;
//                         cout << "single_coeffs[score.first]: " << single_coeffs[score.first] << endl;

//                     }
//                     assert(!isinf(score.second));
//                     assert(!isinf(single_coeffs[score.first]));
//                     break;
//                 }
//                 // Spectra of charge 2
//                 case (2): {
//                     d_score += score.second * double_coeffs[score.first];
//                     assert(!isnan(score.second));
//                     assert(!isnan(double_coeffs[score.first]));

//                     assert(!isinf(score.second));
//                     assert(!isinf(double_coeffs[score.first]));
//                     break;
//                 }
//                 // Spectra of charge 3
//                 case (3): {
//                     d_score += score.second * triple_coeffs[score.first];
//                     assert(!isnan(score.second));
//                     assert(!isnan(triple_coeffs[score.first]));

//                     assert(!isinf(score.second));
//                     assert(!isinf(triple_coeffs[score.first]));
//                     break;
//                 }
//                 // Spectra of all other charges
//                 default: {
//                     d_score += score.second * triple_coeffs[score.first];
//                     if(isnan(score.second)){
//                         cout << "Search score val is nan" << endl;
//                         cout << spectra.sequence << endl;
//                         cout << "Scores: ";
//                         for(auto s : spectra.scores){
//                             cout << s.first << ", " << s.second << "| ";
//                         }
//                         cout << endl;
//                     }
//                     assert(!isnan(score.second));
                    
//                     assert(!isnan(triple_coeffs[score.first]));
//                     break;
//                 }
//             }
//         }
//         assert(!isnan(d_score));
//         assert(!isinf(d_score));
//         // Including mass difference and constant coefficient
//         // in the discriminant function
//         switch (charge) {
//             case (1):{
//                 d_score += (current_mass_diff * single_coeffs["mass_diff"]);
//                 d_score += single_coeffs["constant"];
                
// //                auto md = current_mass_diff;
// //                auto mc = single_coeffs["mass_diff"];
                
//                 break;
//             }
//             case (2):{
//                 d_score += (current_mass_diff * double_coeffs["mass_diff"]);
//                 d_score += double_coeffs["constant"];
                
// //                auto md = current_mass_diff;
// //                auto mc = double_coeffs["mass_diff"];
//                 break;

//             }
//             case (3):{
//                 d_score += (current_mass_diff * triple_coeffs["mass_diff"]);
//                 d_score += triple_coeffs["constant"];
                
// //                auto md = current_mass_diff;
// //                auto mc = triple_coeffs["mass_diff"];
//                 break;
//             }
//             default:{
//                 d_score += (current_mass_diff * triple_coeffs["mass_diff"]);
//                 d_score += triple_coeffs["constant"];
//                 break;
//             }
//         }
//         // Storing the discriminant store with the spectra
//         spectra.discriminant_score = d_score;
//         assert(!isnan(d_score));
    // }
}

#endif

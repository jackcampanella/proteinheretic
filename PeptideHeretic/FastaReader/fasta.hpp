#ifndef FASTA_HPP
#define FASTA_HPP

#pragma once

#include <fstream>
#include <iostream>
#include <vector>
#include <algorithm>

using std::ifstream;
using std::string;
using std::vector;

// Remove after testing
using std::cout;
using std::endl;

class Fasta {
   public:
    // Struct representing a fasta record that contains
    // information particular to a protein in the file
    struct record {
       public:
        string protein_name;
        string sequence;
        string header;

        // Optional information
        string description;
        string entryname;
        string identifier;
        string db;
    };
    string filename;
    int num_records;
    vector<record> records;

    Fasta();
    Fasta(string &file) {
        this->filename = file;
        this->num_records = 0;
        this->read_fasta(file);
    }

   protected:
    void read_fasta(string &fasta_file);
    bool on_header(string &line);
    bool on_sequence(string &line);
    std::pair<string, string> parse_header(string &header);
    void store_record(string &header, string &sequence);
    string remove_carat(string &header);
};

#endif

/*----------------------------------------------------------------------
    Facilitates the parsing of the fasta file and storing each entry
-----------------------------------------------------------------------*/
void Fasta::read_fasta(string &fasta_file) {
    ifstream file(fasta_file);

    if (file.is_open()) {
        string line;
        string current_header = "";
        string current_sequence = "";

        // Iterating through the file line by line
        while (getline(file, line)) {
            // Each time we enter this code block, it means we are beginning
            // to parse a new record
            if (on_header(line)) {
                if (current_header != "" && current_sequence != "") {
                    this->store_record(current_header, current_sequence);
                }
                // Clearing previous record information
                current_sequence.clear();
                current_header.clear();

                // Assigning current header
                current_header = line;
            } else if (on_sequence(line)) {
                // Until we reach the next header or empty line,
                // we append the lines to a current sequence
                current_sequence.append(line);
            }
        }
        // After closing the file, the final record needs
        // to be stored
        file.close();
        this->store_record(current_header, current_sequence);
    }
}

/*-----------------------------------------------------------
    Creates and stores a new record based off of the params.
-------------------------------------------------------------*/
void Fasta::store_record(string &header, string &sequence){
    record new_record;
    new_record.header = header;
    new_record.sequence = sequence;

    // parse header for description and name
    auto h = this->parse_header(header);
    new_record.protein_name = h.first;
    new_record.description = h.second;

    Fasta::records.emplace_back(new_record);
    this->num_records++;
}

/*------------------------------------------------------------------------------
    Indicate whether or not we are on the header portion of a given fasta entry
--------------------------------------------------------------------------------*/
bool Fasta::on_header(string &line) {
    return (line[0] == '>');
}

/*--------------------------------------------------------------------------------
    Indicates whether or not we are on the sequence portion of a give fasta entry
---------------------------------------------------------------------------------*/
bool Fasta::on_sequence(string &line) {
    return (line[0] != '>' && !line.empty());
}

/*---------------------------------------------------------------------------------------
    Parses the protein name and description from the record's header. We consider
    all information after the first space of the header to be part of the description.
-----------------------------------------------------------------------------------------*/
std::pair<string, string> Fasta::parse_header(string &header){
    string name = "";
    string desc = "";
    
    header = remove_carat(header);

    std::size_t space = header.find_first_of(" ");

    if(space != std::string::npos){
        name = header.substr(0, space);
        desc = header.substr(space, header.size());
    } else{
        name = header;
    }

    return(std::make_pair(name, desc));
}

/*---------------------------------------------------------
    Removes the new entry indicator ('>') from the header
----------------------------------------------------------*/
string Fasta::remove_carat(string &header){
    auto carat_location = header.find('>');
    if(carat_location != std::string::npos){
        header.erase(carat_location, 1);
    }
    return(header);
}




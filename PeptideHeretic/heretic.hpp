/*
  
   ___           __  _    __    __ __            __  _    
  / _ \___ ___  / /_(_)__/ /__ / // /__ _______ / /_(_)___
 / ___/ -_) _ \/ __/ / _  / -_) _  / -_) __/ -_) __/ / __/
/_/   \__/ .__/\__/_/\_,_/\__/_//_/\__/_/  \__/\__/_/\__/ 
        /_/                                               
 
*/

#ifndef PEPTIDE_HERETIC_HPP
#define PEPTIDE_HERETIC_HPP

#include <iostream>
#include <unordered_map>
#include <vector>

#include "FastaReader/fasta.hpp"
#include "PepXMLParser/pepxml.hpp"
#include "PepXMLParser/rapidxml-1.13/rapidxml.hpp"
#include "PepXMLParser/rapidxml-1.13/rapidxml_print.hpp"
#include "dfa_utils.hpp"
#include "progress.hpp"
#include "heretic_utils.hpp"

// TODO: Remove after pub
#include "JSON/json.hpp"
using json = nlohmann::json;
using std::ofstream;

using std::string;
using std::unordered_map;
using std::vector;
namespace PeptideHeretic {
    class Heretic {
       public:
        string pepxml;
        string fasta_file;
        string search_method;
        vector<spectra> all_spectra;
        vector<charge_bin> charge_bins;
        bool show_results;
        int iteration = 0;
        int tot_iterations = 0;
        int min_charge = 10, max_charge = 0;
        int min_ntt = 10, max_ntt = 0;
        int min_nmc = 10, max_nmc = 0;
        float min_mass = 10.0, max_mass = 0.0;
        float d_score_threshold = 0.0;

        Heretic();
        Heretic(vector<search_data> &search_results, string &search_method, string &pepxml_file, string &fasta_file, bool show_results=false);

       protected:
        // Discriminant function analysis
        void discriminant_analysis();

        // Mixture Model Distributions
        void discriminant_distribution();
        void compute_ntt_distribution();
        void compute_nmc_distribution();
        void compute_mass_distribution();

        // Probability
        void init_probability();
        void estimate_probability();
        void compute_probability();

        // Binning
        vector<bin<int>> build_ntt_bins(vector<int> const &spectra_indexes);
        vector<bin<int>> build_nmc_bins(vector<int> const &spectra_indexes);
        vector<bin<float>> build_mass_bins(vector<int> const &spectra_indexes);

        // Miscellaneous Helpers
        void convert_searchdata(vector<search_data> &search_results);
        bool check_convergence();
        void build_charge_bins();
        void clean();
        void find_protein_matches();
        std::pair<float, float> get_init_priors();
        std::pair<float, float> get_priors();
        void done();

        // Remove after pub
        void output_json();
        void output_xml();

        // Performs an initial estimate of probabilities, priors, and distributions
        void init() {
            cout << "\n- Training Peptide Model - " << this->all_spectra.size() << " spectra" << endl;
            // Model initialization process
            this->init_probability();
        }

        // Trains our model until convergence is reached
        void train() {
            int current_iteration = 1;
            const int MAX_ITERATIONS = 500;
            bool convergence_reached = false;
            
            progresscpp::ProgressBar progressbar(MAX_ITERATIONS - 1, 50);

            while (current_iteration < MAX_ITERATIONS) {
                this->iteration = current_iteration;
                // Model learning process
                this->discriminant_distribution();
                this->compute_ntt_distribution();
                this->compute_nmc_distribution();
                this->compute_mass_distribution();
                this->estimate_probability();

                // Checking distribution convergence
                convergence_reached = this->check_convergence();
                this->tot_iterations += 1;

                // If we've reached convergence then we can exit the learning process
                if (convergence_reached) {
                    current_iteration = MAX_ITERATIONS;
                    progressbar.complete();
                } else {
                    current_iteration += 1;
                    ++progressbar;
                }
                progressbar.display();
            }
            // Post processing
            progressbar.done();
            this->done();
        }
    };
#endif
    /*-------------------------------------------------------------
                PeptideHeretic's main constructor
    --------------------------------------------------------------*/
    inline Heretic::Heretic(vector<search_data> &search_results, string &search_method, string &pepxml_file, string &fasta_file, bool show_results){
        this->show_results = show_results;
        this->search_method = search_method;
        this->pepxml = pepxml_file;
        this->fasta_file = fasta_file;

        cout << "In peptide heretic: here's pepxml file: " << this->pepxml << endl;

        this->convert_searchdata(search_results);
        this->discriminant_analysis();
        this->build_charge_bins();

        for (auto &bin : this->charge_bins) {
           bin.ntt_bins = build_ntt_bins(bin.spectra_indexes);
           bin.nmc_bins = build_nmc_bins(bin.spectra_indexes);
           bin.mass_bins = build_mass_bins(bin.spectra_indexes);
        }
        this->init();
        this->train();
    }

    /*----------------------------------------------------------------------------------------------------
     Function: discriminant_analysis()
     Performs Discriminant Function analysis on our dataset. Transforms particular search scores for
     each result, Builds a discriminant function for each assignment and then computes a discriminant
     score using this discriminant function. Helper functions can be found in discriminant_analysis.hpp
    -----------------------------------------------------------------------------------------------------*/
    inline void Heretic::discriminant_analysis() {

        for (auto &spectra : this->all_spectra) {
            transform_scores(spectra);
        }
        
        // Converts all search scores into a single discriminant score
        discriminant_function(this->search_method, this->all_spectra);
        float min_d = 10, max_d = 0;
        for (auto &spectra : this->all_spectra){
            spectra.discriminant_score < min_d ? min_d = spectra.discriminant_score : 0;
            spectra.discriminant_score > max_d ? max_d = spectra.discriminant_score : 0;
        }
        cout << "Min d score: " << min_d << endl;
        cout << "Max d score: " << max_d << endl;
    }
    
    /*-----------------------------------------------------------------------------------------------
     Function: init_probability()
     Performs an initial probability estimate for spectra in our dataset. This calculation
     solely considers the datasets current priors and p(F|+)/p(F|-) distributions. It does not
     consider other mixture model distributions. See equation 3 in docs.
    ------------------------------------------------------------------------------------------------*/
    inline void Heretic::init_probability() {
        
        // Prior probability estimates
        float pos_prior = 0.0, neg_prior = 0.0;
        std::pair<float, float> p = get_init_priors();
        pos_prior = p.first;
        neg_prior = p.second;
        
        // Negative discriminant score distributions are initialized
        // with the distribution derived from data with NTT = 0
        float d_neg = PeptideHeretic::derive_dneg(&this->all_spectra);
        //TODO: change
        float neg_mean = PeptideHeretic::get_discrim_threshold(&this->all_spectra);
        this->d_score_threshold = neg_mean;
        
        // Iterating through peptides grouped by charge
        for (charge_bin &charge_bin : this->charge_bins) {
            PeptideHeretic::probability_table probability_table;
            float min_d = 10.0, max_d = 0.0;
            for(auto index : charge_bin.spectra_indexes){
                auto *spc = &this->all_spectra[index];
                if(spc->discriminant_score < min_d){min_d = spc->discriminant_score;}
                if(spc->discriminant_score > max_d){max_d = spc->discriminant_score;}
            }
            
            // Storing discrim score threshold
            charge_bin.discrim_score_threshold = neg_mean;

            assert(!isnan(pos_prior) && !isnan(neg_prior));  //TODO: Remove

            // Then we compute the initial probability using initial estimates of discriminant score
            // distributions and prior probabilities
            for (auto index : charge_bin.spectra_indexes) {
                spectra *spc = &this->all_spectra[index];
                
                float d_pos = PeptideHeretic::derive_dpos(spc, &probability_table);
                
                assert(!isnan(d_pos) && !isnan(d_neg));
                assert(!isnan(pos_prior) && !isnan(neg_prior));
                
                if(spc->discriminant_score <= this->d_score_threshold){
                    this->all_spectra[index].probability = 0.0;
                }
                else{
                    if(d_pos == 0){
                        this->all_spectra[index].probability = 0.001;
                    }else{
                        this->all_spectra[index].probability = (d_pos * pos_prior) /
                        ((d_pos * pos_prior) + (d_neg * neg_prior));
                    }
                }
                
                // TODO: remove after testing
                this->all_spectra[index].all_pos_ds.push_back(d_pos);
                this->all_spectra[index].all_neg_ds.push_back(d_neg);

                assert(!isnan(this->all_spectra[index].probability));

                // Storing probabilities and distribution values
                this->all_spectra[index].init_probability = this->all_spectra[index].probability;
                this->all_spectra[index].d_pos = d_pos;
                this->all_spectra[index].d_neg = d_neg;
            }
        }
    }
    
    /*-------------------------------------------------------------
     
    --------------------------------------------------------------*/
    inline void Heretic::estimate_probability(){
        // Computing priors
        float pos_prior = 0.0, neg_prior = 0.0;
        std::pair<float, float> p = get_priors();
        pos_prior = p.first;
        neg_prior = p.second;

        // Iterating through spectra grouped by charged
        for (charge_bin &charge_bin : this->charge_bins) {
            for (auto index : charge_bin.spectra_indexes) {
                spectra *spc = &this->all_spectra[index];
                float new_p = 0.0;

                if (spc->discriminant_score < this->d_score_threshold) {
                    new_p = 0.0;
                } else {
                    assert(!isnan(spc->d_pos) && !isnan(spc->d_neg));
                    assert(!isnan(spc->ntt_pos) && !isnan(spc->ntt_neg));
                    assert(!isnan(spc->nmc_pos) && !isnan(spc->nmc_neg));
                    assert(!isnan(spc->m_pos) && !isnan(spc->m_neg));
    
                    new_p = PeptideHeretic::probability_equation(spc, pos_prior, neg_prior);
                }

                // Storing new probability
                this->all_spectra[index].probability = new_p;

                // TODO: Remove after testing
                if (this->all_spectra[index].all_probs.empty()) {
                    this->all_spectra[index].all_probs.push_back(this->all_spectra[index].probability);
                    this->all_spectra[index].all_probs.push_back(new_p);
                } else {
                    this->all_spectra[index].all_probs.push_back(new_p);
                }
            }
        }
    }
    
    /*-------------------------------------------------------------
        
    --------------------------------------------------------------*/
    inline void Heretic::compute_probability() {
        // Prior probabilities
        float pos_prior = 0.0, neg_prior = 0.0;
        std::pair<float, float> p = get_priors();
        pos_prior = p.first;
        neg_prior = p.second;
        
        // Iterating through all spectra and computing pbty using final pbty equation
        for(auto &spc : this->all_spectra){
            if(spc.discriminant_score < this->d_score_threshold){
                spc.probability = 0.0;
            }else{
                spc.probability = PeptideHeretic::final_p_equation(&spc, pos_prior, neg_prior);
            }
            
        }
    }

    /*---------------------------------------------------------------------------------------------
     Performs discriminant score distributions. Correct assignments tend to follow a
     Gaussian whereas incorrect assignments tend to follow a Gamma distribution. Helper functions
     can be found in Heretic_utils.hpp Note, each charge state contains
     separate distributions.
    -----------------------------------------------------------------------------------------------*/
    inline void Heretic::discriminant_distribution() {
        for (charge_bin &bin : this->charge_bins) {
            // Retaining a vector of pointers to spectra members in this bin
            vector<spectra *> spectra_ptrs;
            for (auto index : bin.spectra_indexes) {
                spectra *spectra_ptr = &this->all_spectra[index];
                spectra_ptrs.emplace_back(spectra_ptr);
            }

            // Instantiating a 'Heretic math' object with our vector of pointers
            PeptideHeretic::Heretic_Math<float> m(spectra_ptrs);

            // Storing current params for the charge bin
            bin.mu = m.mean;
            bin.sigma = m.stdev;
            bin.alpha = m.alpha;
            bin.beta = m.beta;

            // Iterating through all members of the charge bin
            for (auto index : bin.spectra_indexes) {
                float d_pos = 0.0, d_neg = 0.0;
                float score = this->all_spectra[index].discriminant_score;

                // Plugging score into pdf's to retain p(D|+) and p(D|-)
                d_pos = PeptideHeretic::Heretic_Math<float>::gaussian(score, m.mean, m.stdev);
                d_neg = PeptideHeretic::Heretic_Math<float>::gamma(score, m.mean, m.min, m.alpha, m.beta);

                assert(!isnan(d_pos) && !isnan(d_neg));

                // Storing p(D|+) and p(D|-)
                this->all_spectra[index].d_pos = d_pos;
                this->all_spectra[index].d_neg = d_neg;

                //TODO: Remove after testing
                this->all_spectra[index].all_pos_ds.push_back(this->all_spectra[index].d_pos);
                this->all_spectra[index].all_neg_ds.push_back(this->all_spectra[index].d_neg);
            }
        }
    }

    /*-----------------------------------------------------------------------------------------------------
     Computes p(NTT|+) and p(NTT|-) distributions. For each charge group, spectra will be separated
     further in accordance to shared NTT values. These distributions are computed according to equation 12.
    ------------------------------------------------------------------------------------------------------*/
    inline void Heretic::compute_ntt_distribution() {
        // Computing priors
        float pos_prior = 0.0, neg_prior = 0.0;
        std::pair<float, float> p = get_priors();
        pos_prior = p.first;
        neg_prior = p.second;

        for (charge_bin &charge_bin : this->charge_bins) {
        
            // Checking if this charge bin's ntt distribution model has reached
            // convergence before processing
            if(!charge_bin.ntt_converged){
                // Storing vector of pairs which include each ntt bin's
                // current p(NTT|+) and p(NTT|-)
                vector<std::pair<float, float> > curr_distributions;
                
                // Then we compute p(NTT|+) and p(NTT|-) for each ntt bin
                for (auto &ntt_bin : charge_bin.ntt_bins) {
                    float ntt_pos = 0;
                    float ntt_neg = 0;

                    for (auto index : ntt_bin.member_indexes) {
                        spectra *current = &this->all_spectra[index];

                        assert(index == current->index);
                        assert(this->all_spectra[index].sequence == current->sequence);
                        assert(!isnan(current->probability));

                        ntt_pos += (float)current->probability;
                        ntt_neg += (1.0 - (float)current->probability);
                    }

                    assert(!isnan(ntt_pos) && !isnan(ntt_neg));

                    // Incorporating the proportion of priors and the number of assigned peptides with current value of ntt
                    // Also, taking precuations to avoid nan values. Returning small values rather than 0's
                    (ntt_pos > 0) ? ntt_pos /= ((float)ntt_bin.member_indexes.size() * pos_prior) : ntt_pos = 0.0001;
                    (ntt_neg > 0) ? ntt_neg /= ((float)ntt_bin.member_indexes.size() * neg_prior) : ntt_neg = 0.0001;

                    (ntt_pos < 0.0001) ? ntt_pos = 0.0001 : 0;
                    (ntt_neg < 0.0001) ? ntt_neg = 0.0001 : 0;
                    
                    // Storing current distr. values in vector of pairs - for convergence checking
                    auto dist_pair = std::make_pair(ntt_pos, ntt_neg);
                    curr_distributions.push_back(dist_pair);

                    // Update spectra with computed distributions
                    for (auto index : ntt_bin.member_indexes) {
                        this->all_spectra[index].ntt_pos = ntt_pos;
                        this->all_spectra[index].ntt_neg = ntt_neg;

                        //TODO: Remove after testing
                        this->all_spectra[index].all_pos_ntts.push_back(ntt_pos);
                        this->all_spectra[index].all_neg_ntts.push_back(ntt_neg);
                    }
                }
                // Checking if this ntt model has reached convergence and updating accordingly
                auto convergence_val = PeptideHeretic::check_mixmodel_convergence(charge_bin.ntt_prev_avg, &curr_distributions);
                (convergence_val == 0) ? charge_bin.ntt_converged = true : charge_bin.ntt_prev_avg = convergence_val;
            }
        }
    }

    /***************************************************************************************************************************
    * Function: compute_nmc_distribution()
    * Computes p(NMC|+) and p(NMC|-) distributions. For each charge group, spectra will be separated 
    * further in accordance to shared NMC values. These distributions are computed according to a modified
    * version of equation 12 in PeptideHeretic's publicaton.
    * 
    * TODO: Need to have something that doesn't process bins with 0 members
    ******************************************************************************************************************************/
    /*-----------------------------------------------------------------------------------------------------
 
     
    ------------------------------------------------------------------------------------------------------*/
    inline void Heretic::compute_nmc_distribution() {
        // Computing priors
        float pos_prior = 0.0, neg_prior = 0.0;
        std::pair<float, float> p = get_priors();
        pos_prior = p.first;
        neg_prior = p.second;

        // Iterating through spectra grouped by charged
        for (charge_bin &charge_bin : this->charge_bins) {
            
            // Checking if convergence has been reached for this charge bin's
            // NMC model before processing
            if(!charge_bin.nmc_converged){
                // Storing vector of pairs which include each nmc bin's
                // current p(nmc|+) and p(nmc|-)
                vector<std::pair<float, float> > curr_distributions;
                
                // Then we compute p(NMC|+) and p(NMC|-) for each nmc bin
                for (auto &nmc_bin : charge_bin.nmc_bins) {
                    float nmc_pos = 0.0, nmc_neg = 0.0;

                    // Summing over probabilities of spectra
                    for (auto index : nmc_bin.member_indexes) {
                        spectra *current = &this->all_spectra[index];
                        assert(current->index == index);
                        assert(this->all_spectra[index].sequence == current->sequence);
                        assert(!isnan(current->probability));

                        nmc_pos += (float)current->probability;
                        nmc_neg += (1.0 - (float)current->probability);
                    }

                    assert(!isnan(nmc_pos) && !isnan(nmc_neg));
                    assert(!isnan(pos_prior) && !isnan(neg_prior));

                    // Taking precautions to avoid nan's - returning a small value rather than 0.
                    (nmc_pos > 0.0) ? nmc_pos /= ((float)nmc_bin.member_indexes.size() * pos_prior) : nmc_pos = 0.0001;
                    (nmc_neg > 0.0) ? nmc_neg /= ((float)nmc_bin.member_indexes.size() * neg_prior) : nmc_neg = 0.0001;

                    (nmc_pos < 0.0001) ? nmc_pos = 0.0001 : 0;
                    (nmc_neg < 0.0001) ? nmc_neg = 0.0001 : 0;
                    
                    assert(nmc_pos > 0 && nmc_neg > 0);
                    assert(!isnan(nmc_pos) && !isnan(nmc_neg));

                    // Storing current values of nmc distributions - for convergence checking
                    auto dist_pair = std::make_pair(nmc_pos, nmc_neg);
                    curr_distributions.push_back(dist_pair);
                    
                    // Update spectra with current distributions
                    for (auto index : nmc_bin.member_indexes) {
                        this->all_spectra[index].nmc_pos = nmc_pos;
                        this->all_spectra[index].nmc_neg = nmc_neg;
                    }
                }
                // Checking if this nmc model has reached convergence and updating accordingly
                auto convergence_val = PeptideHeretic::check_mixmodel_convergence(charge_bin.nmc_prev_avg, &curr_distributions);
                (convergence_val == 0) ? charge_bin.nmc_converged = true : charge_bin.nmc_prev_avg = convergence_val;
            }
        }
    }

    /*-----------------------------------------------------------------------------------------------------
     Function: compute_mass_distribution() - See equation 12 (modified)
     Computes p(M|+) and p(M|-) that is, the likelihood that an assignment is correct vs. incorrect
     based on the given mass difference value. Spectra are further grouped together by common mass_diff
     values and distributions are calculated using.
    -----------------------------------------------------------------------------------------------------*/
    inline void Heretic::compute_mass_distribution() {
        // Computing priors
        float pos_prior = 0.0, neg_prior = 0.0;
        std::pair<float, float> p = get_priors();
        pos_prior = p.first;
        neg_prior = p.second;
        // Iterating through spectra grouped by charge
        for (charge_bin &charge_bin : this->charge_bins) {
            
            // Checking if convergence has been reached for this charge bin's
            // mass model before processing
            if(!charge_bin.mass_converged){
                // Storing vector of pairs which include each mass bin's
                // current p(mass|+) and p(mass|-)
                vector<std::pair<float, float> > curr_distributions;
                
                // Computing p(mass|+) and p(mass|-) for each mass bin
                for (auto &mass_bin : charge_bin.mass_bins) {
                    float mass_pos = 0, mass_neg = 0;

                    // Summation over spectra probabilities
                    for (auto index : mass_bin.member_indexes) {
                        spectra *spc = &this->all_spectra[index];
                        assert(index == spc->index);
                        assert(!isnan(spc->probability));
                        mass_pos += spc->probability;
                        mass_neg += (1.0 - spc->probability);
                    }

                    assert(!isnan(mass_pos) && !isnan(mass_neg));
                    // Taking nan precautions - returning small values rather than 0's
                    (mass_pos > 0) ? mass_pos /= ((float)mass_bin.member_indexes.size() * pos_prior) : mass_pos = 0.0001;
                    (mass_neg > 0) ? mass_neg /= ((float)mass_bin.member_indexes.size() * neg_prior) : mass_neg = 0.0001;

                    (mass_pos < 0.0001) ? mass_pos = 0.0001 : 0;
                    (mass_neg < 0.0001) ? mass_neg = 0.0001 : 0;
                    
                    // Storing current values of mass distributions - for convergence checking
                    auto dist_pair = std::make_pair(mass_pos, mass_neg);
                    curr_distributions.push_back(dist_pair);
                    

                    // Updating spectra with distributions
                    for (auto index : mass_bin.member_indexes) {
                        this->all_spectra[index].m_pos = mass_pos;
                        this->all_spectra[index].m_neg = mass_neg;
                    }
                }
                // Checking if this mass model has reached convergence and updating accordingly
                auto convergence_val = PeptideHeretic::check_mixmodel_convergence(charge_bin.mass_prev_avg, &curr_distributions);
                (convergence_val == 0) ? charge_bin.mass_converged = true : charge_bin.mass_prev_avg = convergence_val;
            }
        }
    }

    /*----------------------------------------------------------------------------------------
     Function: build_charge_bins()
     Dynamically builds the charge bins within the range of the charges in given dataset.
     Places spectra in corresponding charge bin with respect to their charge.
    -----------------------------------------------------------------------------------------*/
    inline void Heretic::build_charge_bins() {

        cout << "...Detected peptides in charge states of range: " << this->min_charge << "-" << this->max_charge << endl;

        // Creates bins with respect to min and max charge
        for (int i = this->min_charge; i <= this->max_charge; ++i) {
            charge_bin new_bin(i);
            this->charge_bins.push_back(new_bin);
        }

        for (auto const &spc : this->all_spectra) {
            for (auto &bin : this->charge_bins) {
                if (spc.charge == bin.charge) {
                    bin.spectra_indexes.push_back(spc.index);
                    ++bin.num_spectra;
                    break;
                }
            }
        }
    }

    /*-----------------------------------------
     
    ------------------------------------------*/
    inline vector<bin<int>> Heretic::build_ntt_bins(vector<int> const &spectra_indexes) {
        vector<bin<int>> ntt_bins;

        assert(spectra_indexes.size() > 0);

        // Building bins based off of min and max ntt in dataset
        for (int i = this->min_ntt; i <= this->max_ntt; ++i) {
            bin<int> new_bin(i);
            ntt_bins.push_back(new_bin);
        }

        // Adding Spectras' indexes to ntt bin
        for (auto index : spectra_indexes) {
            spectra *current = &this->all_spectra[index];
            assert(current->index == index);

            // Finding which ntt bin this spectra belongs to
            for (auto &ntt_bin : ntt_bins) {
                if (current->num_termini == ntt_bin.identifier) {
                    ntt_bin.member_indexes.push_back(current->index);
                    break;
                }
            }
        }
        return (ntt_bins);
    }

    /*-----------------------------------------------------------------
    
        
    ------------------------------------------------------------------*/
    inline vector<bin<int>> Heretic::build_nmc_bins(vector<int> const &spectra_indexes) {
        vector<bin<int>> nmc_bins;

        assert(spectra_indexes.size() > 0);

        for (int i = this->min_nmc; i <= this->max_nmc; ++i) {
            bin<int> new_bin(i);
            nmc_bins.emplace_back(new_bin);
        }

        for (auto index : spectra_indexes) {
            spectra *current = &this->all_spectra[index];
            assert(current->index == index);

            // Finding which nmc bin this spectra belongs to
            for (auto &nmc_bin : nmc_bins) {
                if (current->num_missed_cleavs == nmc_bin.identifier) {
                    nmc_bin.member_indexes.push_back(current->index);
                    break;
                }
            }
        }
        return (nmc_bins);
    }

    /*-----------------------------------------------------------------
    
        
    ------------------------------------------------------------------*/
    inline vector<bin<float>> Heretic::build_mass_bins(vector<int> const &spectra_indexes) {
        vector<bin<float>> mass_bins;

        assert(spectra_indexes.size() > 0);

        for (float i = this->min_mass; i <= this->max_mass; ++i) {
            bin<float> new_bin(i, i + 1.0);
            mass_bins.emplace_back(new_bin);
        }

        for (auto index : spectra_indexes) {
            spectra *current = &this->all_spectra[index];
            assert(index == current->index);

            // Finding which bin this spectra belongs to
            for (auto &mass_bin : mass_bins) {
                if (current->mass_diff >= mass_bin.min && current->mass_diff < mass_bin.max) {
                    mass_bin.member_indexes.push_back(current->index);
                    break;
                }
            }
        }
        return (mass_bins);
    }

    /******************************************************************************************
     * Function: clean()
     * Removes spectra from our results that do not pass the specified probability threshold
     * The default threshold is 0.05. Uses the erase remove idiom by way of lambda.
            This can be removed. No longer wasting a step on cleaning.
    *******************************************************************************************/
    inline void Heretic::clean() {
//        float threshold = 0.05;
//        cout << "... Cleaning peptides" << endl;
//        for(auto &spc : this->all_spectra){
//            if(spc.probability < threshold){
//                spc.noise == true;
//            }
//        }
    }

    /*----------------------------------------------------------------------
     Function: convert_searchdata()
     Converts results pulled from pepxml file into a vector of type spectra
    ------------------------------------------------------------------------*/
    inline void Heretic::convert_searchdata(vector<search_data> &search_results) {
        for (auto &result : search_results) {
            // Maintaining index of the location of the spectra to be added
            // When adding an item to the vector, the index of the item to be added
            // is equal to the current size of the vector (since vector's begin at 0)
            int index = (int)this->all_spectra.size();

            // Main peptide information
            spectra new_spectra(result.peptide_sequence, result.charge, index);

            // Peptide auxillary information
            new_spectra.num_termini = result.num_termini;
            new_spectra.num_missed_cleavs = result.num_missed_cleavs;
            new_spectra.total_num_ions = result.total_num_ions;
            new_spectra.num_matched_ions = result.num_matched_ions;
            new_spectra.mass_diff = result.mass_diff;
            new_spectra.scores = result.scores;
            new_spectra.calc_neutral_pep_mass = result.calc_neutral_pep_mass;
            new_spectra.spectrum_id = result.spectrum_id;
            
            // Checking for modification info
            if(result.has_mod_info){
                new_spectra.has_mod_info = true;
                new_spectra.modified_peptide = result.modified_peptide;
                new_spectra.mod_info = result.mod_info;

                // Check for nterm mod
                if(result.has_nterm_mod){
                    new_spectra.has_nterm_mod = true;
                    new_spectra.nterm_mass = result.nterm_mass;
                }

                // Check for cterm mod
                if(result.has_cterm_mod){
                    new_spectra.has_cterm_mod = true;
                    new_spectra.cterm_mass = result.cterm_mass;
                }
            }
            
            // Matched protein information
            new_spectra.matched_protein = result.matched_protein;
            new_spectra.num_matched_proteins = result.num_matched_proteins;

            // Checking min/max charge
            (new_spectra.charge < this->min_charge) ? this->min_charge = new_spectra.charge : 0;
            (new_spectra.charge > this->max_charge) ? this->max_charge = new_spectra.charge : 0;

            // Checking min/max ntt
            (new_spectra.num_termini < this->min_ntt) ? this->min_ntt = new_spectra.num_termini : 0;
            (new_spectra.num_termini > this->max_ntt) ? this->max_ntt = new_spectra.num_termini : 0;

            // Checking min/max nmc
            (new_spectra.num_missed_cleavs < this->min_nmc) ? this->min_nmc = new_spectra.num_missed_cleavs : 0;
            (new_spectra.num_missed_cleavs > this->max_nmc) ? this->max_nmc = new_spectra.num_missed_cleavs : 0;

            // Checking min/max massd
            (new_spectra.mass_diff < this->min_mass) ? this->min_mass = new_spectra.mass_diff : 0;
            (new_spectra.mass_diff > this->max_mass) ? this->max_mass = new_spectra.mass_diff : 0;

            // Storing in our list of peptides
            this->all_spectra.push_back(new_spectra);

            assert(new_spectra.sequence == this->all_spectra[index].sequence);
        }
    }
    /*-----------------------------------------------------------------
     Function: get_init_priors()
     Computes p(+) and p(-), the prior probabilities of a correct/incorrect assignment.
     Prior probabilities are the overall proportion of correct/incorrect peptides in the dataset.
     TODO: Finish notes
    ------------------------------------------------------------------*/
    inline std::pair<float, float> Heretic::get_init_priors() {
        float pos_prior = 0.0;
        float neg_prior = 0.0;

        for (auto const &spectra : this->all_spectra) {
            if (spectra.num_termini == 0 || spectra.discriminant_score <= 0) {
                ++neg_prior;
            } else {
                ++pos_prior;
            }
        }
        // Nan value precautions
        (!isnan(pos_prior)) ? pos_prior /= (float)this->all_spectra.size() : pos_prior = 0.0;
        (!isnan(neg_prior)) ? neg_prior /= (float)this->all_spectra.size() : neg_prior = 0.0;

        return std::make_pair(pos_prior, neg_prior);
    }
    
    /*-----------------------------------------------------------------
     Function: get_priors()
     TODO: finish notes
        
    ------------------------------------------------------------------*/
    inline std::pair<float, float> Heretic::get_priors() {
        float pos_prior = 0.0;
        float neg_prior = 0.0;

        for (auto const &spectra : this->all_spectra) {
            assert(!isnan(spectra.probability));
            pos_prior += spectra.probability;
            neg_prior += (1.0 - spectra.probability);
        }

        // Nan value precautions
        (!isnan(pos_prior)) ? pos_prior /= (float)this->all_spectra.size() : pos_prior = 0.0;
        (!isnan(neg_prior)) ? neg_prior /= (float)this->all_spectra.size() : neg_prior = 0.0;

        return std::make_pair(pos_prior, neg_prior);
    }

    /*-----------------------------------------------------------------
     Function: find_protein_matches()
     For peptides that have more than 1 total corresponding proteins, we need to determine
     which ones these are and list these as alternative proteins. Fasta functions can be found
     in FastaReader/fasta.hpp
    ------------------------------------------------------------------*/
    inline void Heretic::find_protein_matches() {
        // Reading in fasta records
        Fasta fasta(this->fasta_file);

        for (auto &peptide : this->all_spectra) {
            // If peptide corresponds to more than one protein
            // we search the fasta database for it (naive O(n^2) search)
            if (peptide.num_matched_proteins > 1) {
                for (auto record : fasta.records) {
                    std::size_t found = record.sequence.find(peptide.sequence);
                    if (found != std::string::npos) {
                        // if the current peptide is a substring of the current protein
                        // we store this protein's name as an alternative protein of the peptide
                        peptide.alternative_proteins.push_back(record.protein_name);
                    }
                }
            }
        }
    }

    /*-----------------------------------------------------------------------------------------------------------
     Function: check_convergence()
     If there are no significant changes in the model's distributions then we consider
     convergence to be reached. We consider the absolute value of the difference of the current values of the
     distribution params at iteration i and the values of the parameters at iteration i - 1. If the difference
     is small then we make the conclusion that this process has converged. Each Charge bin has its own set of
     parameters. This method of checking convergence is described in PeptideHeretic's supporting info:
     
     https://bmcbioinformatics.biomedcentral.com/articles/10.1186/1471-2105-13-S16-S1
    ------------------------------------------------------------------------------------------------------------*/
    inline bool Heretic::check_convergence() {
        float avg_param_change = 0.0;

        for (auto &bin : this->charge_bins) {
            float avg_bin_change = 0.0;
            avg_bin_change = (abs(bin.mu - bin.p_mu) + abs(bin.sigma - bin.p_sigma) + abs(bin.alpha - bin.p_alpha) + abs(bin.beta - bin.p_beta)) / 4.0;
            avg_param_change += avg_bin_change;

            bin.p_mu = bin.mu;
            bin.p_sigma = bin.sigma;
            bin.p_alpha = bin.alpha;
            bin.p_beta = bin.beta;
        }
        avg_param_change /= (float)this->charge_bins.size();

        assert(!isnan(avg_param_change));

        if (avg_param_change < 0.001) {
            return (true);
        } else {
            return (false);
        }
    }
    /*-----------------------------------------------------------
     
    ------------------------------------------------------------*/
    inline void Heretic::done(){
        cout << "(iteratons to train model: " << this->tot_iterations << ")\n" << endl;
        this->compute_probability();
        this->clean();
        this->find_protein_matches();

        // Output results in pep.xml if requested
        if(this->show_results){
            this->output_xml();
        }
    }

    /*-----------------------------------------------------------
                    Outputs json results
    ------------------------------------------------------------*/
    inline void Heretic::output_json() {
        string output_file = "peptide_heretic.json";

        ofstream outfile(output_file);
        json results = {};

        for (auto const &spectra : this->all_spectra) {
//            if(spectra.probability > 0.05){
                json peptide = {
                    {"peptide", spectra.sequence},
                    {"probability", spectra.probability},
                    {"scores", spectra.scores},
                    {"ntt", spectra.num_termini},
                    {"nmc", spectra.num_missed_cleavs},
                    {"massd", spectra.mass_diff},
                    {"fval", spectra.discriminant_score}};
                results.push_back(peptide);
//            }
        }
        outfile << std::setw(4) << results << endl;
        outfile.close();

        cout << "\nPeptide Heretic results stored in: " << output_file << endl;
    }

    /*-------------------------------------------------------------------------------------------
        Outputs a new pepxml file with Peptide Heretic analysis included.
        We are essentially taking the input pep.xml's info, appending Protein Heretic analysis
        results to it and then outputting a new pepxml.
    --------------------------------------------------------------------------------------------*/
    inline void Heretic::output_xml(){
        cout << "Writing pepxml file" << endl;

        // We need to make this less slow
            // When we output a peptide, we should be able to mark it as processed or something
            // so that we aren't processing over something we already processed over
        
        // Or maybe there is someway to know the locations of the peptides in the pep.xml

        string output_file = "heretic.pep.xml";
        rapidxml::file<> xmlFile(this->pepxml.c_str());
        rapidxml::xml_document<> doc;
        doc.parse<0>(xmlFile.data());

        xml_node<> *root_node = doc.first_node();
        
        // Creating analysis summary node and prepending it to root node
        xml_node<> *analysis_node = doc.allocate_node(node_element, "analysis_summary");
        xml_attribute<> *analysis_attr = doc.allocate_attribute("analysis", "peptide_heretic");
        analysis_node->append_attribute(analysis_attr);
        root_node->prepend_node(analysis_node);

        // Creating PeptideHeretic summary node and prepending it to analysis summary
        xml_node<> *heretic = doc.allocate_node(node_element, "peptide_heretic_summary");
        xml_attribute<> *version = doc.allocate_attribute("version", "Peptide Heretic C++ 14 (gcc compiler: 8.3.1 20191121 (Red Hat 8.3.1-5) clang version 11.0.3 (clang-1103.0.32.62)");
        heretic->append_attribute(version);
        analysis_node->append_node(heretic);

        // Creating input file node and appending it to heretic summary node
        xml_node<> *input_file_node = doc.allocate_node(node_element, "inputfile");
        char *file_name = doc.allocate_string(this->pepxml.c_str());
        xml_attribute<> *name = doc.allocate_attribute("name", file_name);
        input_file_node->append_attribute(name);
        heretic->append_node(input_file_node);

        for (auto &spectra : this->all_spectra){
            xml_node<> *spectrum = doc.first_node()->first_node("msms_run_summary")->first_node("spectrum_query");

            while(spectrum != NULL){
                // Retaining spectrum attribute from spectrum query node
                string xml_spectrum_id = spectrum->first_attribute("spectrum")->value();

                // If current spectra's spectrum id matches spectrum then
                // we drill into the search hits associated with it
                if(spectra.spectrum_id == xml_spectrum_id){
                    xml_node<> *search_hit = spectrum->first_node("search_result")->first_node("search_hit");    

                    while(search_hit != NULL){
                    string xml_peptide = search_hit->first_attribute("peptide")->value();

                        // If the sequences match then we append our analysis
                        // results to the search hit node
                        if(spectra.sequence == xml_peptide){

                            // Building: <analysis_result analysis="peptide_heretic">
                            xml_node<> *analysis_result = doc.allocate_node(node_element, "analysis_result");
                            xml_attribute<> *heretic_analysis_attr = doc.allocate_attribute("analysis", "peptide_heretic");
                            analysis_result->append_attribute(heretic_analysis_attr);

                            // Building: <peptide_heretic_result probability=""> as child of analysis_result
                            xml_node<> *heretic_result = doc.allocate_node(node_element, "peptide_heretic_result");
                            char *pbty = doc.allocate_string(std::to_string(spectra.probability).c_str());
                            xml_attribute<> *pbty_attr = doc.allocate_attribute("probability", pbty);
                            heretic_result->append_attribute(pbty_attr);
                            analysis_result->append_node(heretic_result);

                            // Building: <search_score_summary> as child of heretic result
                            xml_node<> *search_score_summary = doc.allocate_node(node_element, "search_score_summary");
                            heretic_result->append_node(search_score_summary);

                            // Building <parameter name="" value=""> as child of search score summary
                            char *fval = doc.allocate_string(std::to_string(spectra.discriminant_score).c_str());
                            char *ntt = doc.allocate_string(std::to_string(spectra.num_termini).c_str());
                            char *nmc = doc.allocate_string(std::to_string(spectra.num_missed_cleavs).c_str());
                            char *massd = doc.allocate_string(std::to_string(spectra.mass_diff).c_str());

                            // For fval
                            xml_node<> *param_fval = doc.allocate_node(node_element, "parameter");
                            xml_attribute<> *fval_name = doc.allocate_attribute("name", "fval");
                            xml_attribute<> *fval_val = doc.allocate_attribute("value", fval);
                            param_fval->append_attribute(fval_name);
                            param_fval->append_attribute(fval_val);
                            search_score_summary->append_node(param_fval);

                            // For ntt
                            xml_node<> *param_ntt = doc.allocate_node(node_element, "parameter");
                            xml_attribute<> *ntt_name = doc.allocate_attribute("name", "ntt");
                            xml_attribute<> *ntt_val = doc.allocate_attribute("value", ntt);
                            param_ntt->append_attribute(ntt_name);
                            param_ntt->append_attribute(ntt_val);
                            search_score_summary->append_node(param_ntt);
                            
                            // For nmc
                            xml_node<> *param_nmc = doc.allocate_node(node_element, "parameter");
                            xml_attribute<> *nmc_name = doc.allocate_attribute("name", "nmc");
                            xml_attribute<> *nmc_val = doc.allocate_attribute("value", nmc);
                            param_nmc->append_attribute(nmc_name);
                            param_nmc->append_attribute(nmc_val);
                            search_score_summary->append_node(param_nmc);

                            // For massd
                            xml_node<> *param_massd = doc.allocate_node(node_element, "parameter");
                            xml_attribute<> *massd_name = doc.allocate_attribute("name", "massd");
                            xml_attribute<> *massd_val = doc.allocate_attribute("value", massd);
                            param_massd->append_attribute(massd_name);
                            param_massd->append_attribute(massd_val);
                            search_score_summary->append_node(param_massd);

                            // Appending to search hit node
                            search_hit->append_node(analysis_result);
                        }
                        search_hit = search_hit->next_sibling("search_hit");
                    }
                }
                spectrum = spectrum->next_sibling("spectrum_query");
            }
        }

        // Writing new xml doc to output file
        ofstream outfile(output_file);
        outfile << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" << endl;
        outfile << doc;
        outfile.close();
        cout << "Finished writing pepxml file" << endl;
    }
}

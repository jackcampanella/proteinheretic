/*
  
   ___           __  _    __    __ __            __  _    
  / _ \___ ___  / /_(_)__/ /__ / // /__ _______ / /_(_)___
 / ___/ -_) _ \/ __/ / _  / -_) _  / -_) __/ -_) __/ / __/
/_/   \__/ .__/\__/_/\_,_/\__/_//_/\__/_/  \__/\__/_/\__/ 
        /_/                                               
 
*/
#ifndef PEP_HERETIC_UTILS_HPP
#define PEP_HERETIC_UTILS_HPP
#pragma once

#include <iostream>
#include <unordered_map>
#include <vector>

using std::string;
using std::unordered_map;
using std::vector;

/////////////////////////////////////////////////////////////////////////////////////////////
struct spectra {
    // General Spectra information
    string sequence;
    int index;
    int id;
    string spectrum_id = "";
    float init_probability = 0;
    string matched_protein;
    vector<string> alternative_proteins;
    int num_matched_proteins;
    int charge;
    bool noise = false;

    // General mod info
    bool has_mod_info = false;
    string modified_peptide = "";
    vector<modification_info> mod_info;

    // nterm and cterm mod info
    bool has_nterm_mod = false;
    bool has_cterm_mod = false;
    float nterm_mass = 0.0;
    float cterm_mass = 0.0;
    
    // Auxillary information
    int num_termini, num_missed_cleavs, num_matched_ions, total_num_ions;
    float mass_diff, discriminant_score, normalized_d_score, property_score;
    float calc_neutral_pep_mass = 0.0;

    // Probability and distribution informaiton
    float probability = 0;
    float d_pos = 0, d_neg = 0, ntt_pos = 0, ntt_neg = 0;
    float nmc_pos = 0, nmc_neg = 0, m_pos = 0, m_neg = 0;

    float p_i = 0;

    float init_d_pos = 0;

    // Scores retrieved from search algorithm results
    unordered_map<string, float> scores;

    // TODO: Remove after testing
    vector<float> all_probs;
    vector<float> all_pos_ds;
    vector<float> all_neg_ds;
    vector<float> all_pos_ntts;
    vector<float> all_neg_ntts;

    // Constructors TODO: Use initializer list
    inline spectra(){};
    inline spectra(string &sequence, int charge, int index) : sequence(sequence), charge(charge), index(index) {};
    inline float get_score(string score_name){
        float score_val = 0.0;
        bool score_found = false;
        for(auto &score : this->scores){
            if(score.first == score_name){
                score_val = score.second;
                score_found = true;
                break;
            }
        }
        if(!score_found){
            cout << "[ERR]: The score " << score_name << " does not exist for peptide: " << this->sequence << ", spectrum id: " << this->spectrum_id << endl;
            throw std::exception();
        }
        return(score_val);
    }
};
/////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////
/****************************************************************************************
 * Struct: bin
 * A generic bin that can contain various types of min/max indicators
 * as well as different member id types. This serves as a helpful container
 * when needing to group together items further.
*******************************************************************************************/
template <typename T>
struct bin {
    int index;
    T min;
    T max;
    T identifier;
    vector<T> member_ids;
    vector<T> member_indexes;
    int num_members = 0;
    
    // Distribution probabilities
    float pos_distr = 0.0;
    float neg_distr = 0.0;
    
    // Constructors - Overloads for single identifier
    // and for min/max identifiers
    inline bin(){};
    inline bin(const T &min, const T &max) {
        this->min = min;
        this->max = max;
    }
    inline bin(const T &value) {
        this->identifier = value;
    }
};
/////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////
// Distributions and collective computations are performed
// on groups of spectra of the same charge
struct charge_bin {
    // Identification and membership
    int charge = 0;
    int num_spectra = 0;
    vector<int> spectra_ids;
    vector<int> spectra_indexes;

    // Mixture model bins
    vector<bin<int>> ntt_bins;
    vector<bin<int>> nmc_bins;
    vector<bin<float>> mass_bins;
    
    // Mixture model convergence detection
    float ntt_prev_avg = 0.0;
    bool ntt_converged = false;
    float nmc_prev_avg = 0.0;
    bool nmc_converged = false;
    float mass_prev_avg = 0.0;
    bool mass_converged = false;

//    vector<std::shared_ptr<spectra>> spectra_ptrs;
    
    // Means and priors
    float initial_mean = 0;
    float mean = 0, stdev = 0;
    float pos_prior = 0, neg_prior = 0;

    float discrim_score_threshold = 0;

    // Discriminant Score Model Parameters
    float mu = 0.0, sigma = 0.0, alpha = 0.0, beta = 0.0;
    float p_mu = 0.0, p_sigma = 0.0, p_alpha = 0.0, p_beta = 0.0;

    // Constructors
    inline charge_bin() {}
    inline charge_bin(int &charge) {
        this->charge = charge;
    }

    // Returns the bin to a more initialized state. However, the number of spectra in the bin
    // and the discriminant scores do not change throughout the algorithm
    inline void reset() {
        // this->scores.clear();
        // this->probabilities.clear();
        this->mean = 0;
        this->stdev = 0;
    }
};
/////////////////////////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////////////////////////
/*********************************************************************************************
                            Heretic_Math Class
        A specialized math class that handles various math operations throug-
        out the algorithm. Heretic_Math can act as a static library with
        independent output or it can facilitate the process of computing various
        parameters by just instantiation.

**********************************************************************************************/
namespace PeptideHeretic {
    template <class T>
    class Heretic_Math {
       public:
        // Public member attributes
        T mean = 0.0;
        T stdev = 0.0;
        T min = 0.0;
        T m1 = 0.0;
        T m2 = 0.0;
        T alpha = 0.0;
        T beta = 0.0;
        vector<spectra *> elements;

        Heretic_Math(vector<spectra *> &spectra);

        // Static library functions that can be called for cases when
        // instantiation of the class is unnecessary
        static T compute_mean(vector<spectra *> &spectra);
        static T compute_mean(vector<spectra> *spectra);
        static T compute_stdev(vector<spectra *> &spectra, T mean);
        static T compute_min(vector<spectra *> &spectra);
        static T compute_m1(vector<spectra *> &spectra, T min);
        static T compute_m2(vector<spectra *> &spectra, T min, T m1);
        static T compute_alpha(T m1, T m2);
        static T compute_beta(T m1, T m2);
        static T gaussian(const T &score, const T &mean, const T &stdev);
        static T gamma(const T &score, const T &mean, const T &min, T &alpha, T &beta);
        static T gamma_approx(const T x);

        // Private member functions, internal use only
       private:
        void compute_mean();
        void compute_stdev();
        void compute_m1();
        void compute_m2();
    };

    /*---------------------------------------------------------------
        Primary Heretic_Math constructor: Facilitates the process of 
        computing mean, min, stdev, m1, m2, alpha, beta.
    -----------------------------------------------------------------*/
    template <typename T>
    inline Heretic_Math<T>::Heretic_Math(vector<spectra *> &spectra) {
        // TODO: need to raise exception if this array of pointers is null
        // asserting for now
        assert(spectra.size() > 0);
        this->elements = spectra;
        this->compute_mean();
        this->compute_stdev();
        this->compute_m1();
        this->compute_m2();
    }

    /*------------------------------------------------------------------
        Computes the mean (mu param) in accordance to PeptideHeretic's
        supporting information. Computes teh min as well.
    --------------------------------------------------------------------*/
    template <typename T>
    inline void Heretic_Math<T>::compute_mean() {
        T min = 1000.0;
        T top_sum = 0.0;
        T btm_sum = 0.0;

        for (auto const &e : this->elements) {
            // TODO: account for the possibility of nan values showing up
            // in an attribute that we are summing over

            assert(!isnan(e->probability) && !isnan(e->discriminant_score));

            top_sum += (e->probability * e->discriminant_score);
            btm_sum += e->probability;

            // Retaining min as well
            if (e->discriminant_score < min) {
                min = e->discriminant_score;
            }
        }
        // Storing min
        this->min = min;
        // Storing mean with nan precautionary measures
        (top_sum == 0 || btm_sum == 0) ? this->mean = 0.0 : this->mean = (top_sum / btm_sum);

        assert(!isnan(mean));
    }

    /*--------------------------------------------------------------------
        Computes the standard deviation in accordance to PeptideHeretic's
        supporting information.
    ---------------------------------------------------------------------*/
    template <typename T>
    inline void Heretic_Math<T>::compute_stdev() {
        T top_sum = 0.0;
        T btm_sum = 0.0;

        for (auto const &e : this->elements) {
            assert(!isnan(e->probability) && !isnan(e->discriminant_score));
            assert(!isnan(mean));
            top_sum += e->probability * ((e->discriminant_score - this->mean) * (e->discriminant_score - this->mean));
            btm_sum += e->probability;
        }

        // Storing stdev with nan value precautions
        (top_sum == 0 || btm_sum == 0) ? this->stdev = 0.0 : this->stdev = sqrt(top_sum / btm_sum);
        assert(!isnan(this->stdev));
    }

    /*-------------------------------------------------------------------------------
        Computes the m1 param, eventually used to compute the 
        alpha/beta param, in accordance to PeptideHeretic's supporting information.
    ---------------------------------------------------------------------------------*/
    template <typename T>
    inline void Heretic_Math<T>::compute_m1() {
        T top_sum = 0.0;
        T btm_sum = 0.0;

        for (auto &spectra : this->elements) {
            // TODO: Need to account for possibility of summing over nan values
            top_sum += (1.0 - spectra->probability) * (spectra->discriminant_score - this->min);
            btm_sum += (1.0 - spectra->probability);
        }

        // Storing m1 with nan value precautions
        (top_sum == 0 || btm_sum == 0) ? this->m1 = 0 : this->m1 = (top_sum / btm_sum);
    }

    /*----------------------------------------------------------------
        Computes the m2 param, eventually needed for alpha and beta
        params, in accordance to Heretic's supporting information.
    ------------------------------------------------------------------*/
    template <typename T>
    inline void Heretic_Math<T>::compute_m2() {
        T top_sum = 0.0;
        T btm_sum = 0.0;

        for (auto &spectra : this->elements) {
            // TODO: Need to account for possibility of summing over nan values
            top_sum += (1.0 - spectra->probability) * pow(spectra->discriminant_score - this->min - this->m1, 2);
            btm_sum += (1.0 - spectra->probability);
        }
        // Preventing nan's and storing m2
        if (top_sum != 0 && btm_sum != 0) {
            this->m2 = top_sum / btm_sum;
        }

        // Computing alpha and beta
        if (this->m1 != 0 && this->m2 != 0) {
            this->alpha = (this->m1 * this->m1) / this->m2;
            this->beta = this->m1 / this->m2;
        }
    }

    /*------------------------------------------------------------------
        A static overloaded version of Heretic_Math's mean function.
    -------------------------------------------------------------------*/
    template <typename T>
    inline T Heretic_Math<T>::compute_mean(vector<spectra *> &spectra) {
        T top_sum = 0.0;
        T btm_sum = 0.0;
        T mean = 0.0;

        for (auto &spectra : spectra) {
            // TODO: account for the possibility of nan values showing up
            // in an attribute that we are summing over
            top_sum += (spectra->probability * spectra->discriminant_score);
            btm_sum += spectra->probability;
        }
        // Safeguarding against returning a nan value
        if (top_sum != 0 && btm_sum != 0) {
            mean = (top_sum / btm_sum);
        }

        return (mean);
    }

    /*------------------------------------------------------------------
        A static overloaded version of Heretic_Math's stdev function.
    --------------------------------------------------------------------*/
    template <typename T>
    inline T Heretic_Math<T>::compute_stdev(vector<spectra *> &spectra, T mean) {
        T top_sum = 0.0;
        T btm_sum = 0.0;
        T stdev = 0.0;

        for (auto &spectra : spectra) {
            // TODO: account for the possibility of nan values showing up
            // in an attribute that we are summing over
            top_sum += spectra->probability * ((spectra->discriminant_score - mean) * (spectra->discriminant_score - mean));
            btm_sum += spectra->probability;
        }
        // Safeguarding against returning a nan value
        if (top_sum != 0 && btm_sum != 0) {
            stdev = (top_sum / btm_sum);
            stdev = sqrt(stdev);
        }
        return (stdev);
    }

    /*---------------------------------------------------------------
        Returns the minimum value of the provided spectra elements
    -----------------------------------------------------------------*/
    template <typename T>
    inline T Heretic_Math<T>::compute_min(vector<spectra *> &spectra) {
        T min = 1000.0;
        for (auto &spectra : spectra) {
            // TODO: Need to account for possibility of summing over nan values
            if (spectra->discriminant_score < min) {
                min = spectra->discriminant_score;
            }
        }
        return (min);
    }

    /*---------------------------------------------------------------
        A static overloaded version of Heretic_Math's m1 function
    -----------------------------------------------------------------*/
    template <typename T>
    inline T Heretic_Math<T>::compute_m1(vector<spectra *> &spectra, T min) {
        T m1 = 0.0;
        T top_sum = 0.0;
        T btm_sum = 0.0;
        for (auto &spectra : spectra) {
            // TODO: Need to account for possibility of summing over nan values
            top_sum += (1.0 - spectra->probability) * (spectra->discriminant_score - min);
            btm_sum += (1.0 - spectra->probability);
        }

        // Safeguarding against nan's
        if (top_sum != 0 && btm_sum != 0) {
            m1 = top_sum / btm_sum;
        }
        return (m1);
    }

    /*---------------------------------------------------------------
        A static overloaded version of Heretic_Math's m2 function
    -----------------------------------------------------------------*/
    template <typename T>
    inline T Heretic_Math<T>::compute_m2(vector<spectra *> &spectra, T min, T m1) {
        T m2 = 0.0;
        T top_sum = 0.0;
        T btm_sum = 0.0;

        for (auto &spectra : spectra) {
            // TODO: Need to account for possibility of summing over nan values
            top_sum += (1.0 - spectra->probability) * pow(spectra->discriminant_score - min - m1, 2);
            btm_sum += (1.0 - spectra->probability);
        }

        // Preventing nan return
        if (top_sum != 0 && btm_sum != 0) {
            m2 = top_sum / btm_sum;
        }
        return (m2);
    }

    /*-------------------------------------------------------------------
        Computes alpha based on the 'method of moments' params provided.
    ---------------------------------------------------------------------*/
    template <typename T>
    inline T Heretic_Math<T>::compute_alpha(T m1, T m2) {
        if (m1 != 0 && m2 != 0) {
            return ((m1 * m1) / m2);
        } else {
            return (0.0);
        }
    }

    /*-------------------------------------------------------------------
        Computes beta based on the 'method of moments' params provided.
    ---------------------------------------------------------------------*/
    template <typename T>
    inline T Heretic_Math<T>::compute_beta(T m1, T m2) {
        if (m1 != 0 && m2 != 0) {
            return (m1 / m2);
        } else {
            return (0.0);
        }
    }

    /*-------------------------------------------------------------------
        Computes the Gaussian (Normal) distribution's probability
        density function with respect to the provided params.
        Logic derived from PeptideHeretic's original source code.
    ---------------------------------------------------------------------*/
    template <typename T>
    inline T Heretic_Math<T>::gaussian(const T &score, const T &mean, const T &stdev) {
        T result;

        if (stdev == 0) {
            if (score == mean) {
                return (0.99);
            } else {
                return (0.0);
            }
        }

        static const T inv_sqrt_2pi = 0.3989422804014327;
        T a = (score - mean) / stdev;

        result = inv_sqrt_2pi / stdev * std::exp(-T(0.5) * a * a);

        // Avoiding returning 0 values
        // if (result < 0.01) {
        //     result = 0.01;
        // }
        assert(!isnan(result));
        (result < 0.001) ? result = 0.001 : 0;

        assert(!isnan(result));
        assert(result >= 0.001);

        return (result);
    }

    /*---------------------------------------------------------------------------
        Computes the Gamma distributions probability density function
        with respect to the provided params. Logic derived from PeptideHeretic's
        original source code.
    -----------------------------------------------------------------------------*/
    template <typename T>
    inline T Heretic_Math<T>::gamma(const T &score, const T &mean, const T &min, T &alpha, T &beta) {
        assert(!isnan(score) && !isnan(mean) && !isnan(min) && !isnan(alpha) && !isnan(beta));
        assert(!isinf(score) && !isinf(mean) && !isinf(min) && !isinf(alpha) && !isinf(beta));
        
        const float max_beta = 125.0;
        const float HI_VAL = 100.0;
        const float LOW_VAL = 0.001;
        T result = 0.001;
        T value = 0.0;

        // Parts of the probability density function equation
        T top_first_term = 0.0, top_second_term = 0.0;
        T btm_first_term = 0.0, btm_second_term = 0.0;

        // Adjusting for very large beta
        if (beta > max_beta) {
            beta = max_beta;
            alpha = mean / beta;
        }
        // Beta parameter must be greater than 0
        if (beta <= 0) {
            return (result);
        }

        // The value being plugged into the gamma pdf should be
        // the min val subtracted from the score.
        value = score - min;
        assert(!isnan(value) && !isnan(alpha) && !isnan(beta));
        

        // The gamma distribution requires that all 'x' and parameters
        // are greater than or equal to 0
        if (value > 0 && alpha > 0 && beta > 0) {
            // Computing numerator
            top_first_term = pow(value, (alpha - 1.0));  //TODO: be warry of nan or zero values for these terms and inf values
            (isinf(top_first_term)) ? top_first_term = HI_VAL : 0;
            (isnan(top_first_term)) ? top_first_term = LOW_VAL : 0;

            top_second_term = exp(-value / beta);
            (isinf(top_second_term)) ? top_second_term = HI_VAL : 0;
            (isnan(top_second_term)) ? top_second_term = LOW_VAL : 0;

            // Computing denominator
            btm_first_term = pow(beta, alpha);
            (isinf(btm_first_term)) ? btm_first_term = HI_VAL : 0;
            (isnan(btm_first_term)) ? btm_first_term = LOW_VAL : 0;
            
            btm_second_term = std::tgamma(alpha);
            (isinf(btm_second_term)) ? btm_second_term = HI_VAL : 0;
            (isnan(btm_second_term)) ? btm_second_term = LOW_VAL : 0;
            

            assert(!isnan(top_first_term) && !isnan(top_second_term) && !isnan(btm_first_term) && !isnan(btm_second_term));
            assert(!isinf(top_first_term) && !isinf(top_second_term) && !isinf(btm_first_term) && !isinf(btm_second_term));

            result = (top_first_term * top_second_term) / (btm_first_term * btm_second_term);
        }
        assert(!isnan(result));
        (result < 0.001) ? result = 0.001 : 0;
        return (result);
        
    }

    /*-------------------------------------------------------------------
        Computes an approximation to the gamma function used in
        the computation of the gamma's pdf.
    ---------------------------------------------------------------------*/
    template <typename T>
    inline T Heretic_Math<T>::gamma_approx(const T x) {
        
        T result = (1.000000000190015 +
                    76.18009172947146 / (x + 1) +
                    -86.50532032941677 / (x + 2) +
                    24.01409824083091 / (x + 3) +
                    -1.231739572450155 / (x + 4) +
                    1.208650973866179e-3 / (x + 5) +
                    -5.395239384953e-6 / (x + 6));

        result = result * sqrt(2 * M_PI) / x * pow(x + 5.5, x + .5) * exp(-x - 5.5);


        (isinf(result)) ? result = 100 : result = result;
        

        assert(!isinf(result));
        assert(!isnan(result));

        return (result);
    }

}  // namespace PeptideHeretic

///////////////////////////////////////////////////////////////////////////////////////////////


namespace PeptideHeretic{
    
    /*------------------------------
     TODO:
    -------------------------------*/
    struct probability_table {
        const unordered_map<int, float> NTT_ZERO = {
            {-6, 0.0}, {-5, 0.0}, {-4, 0.0}, {-3, 0.0}, {-2, 0.0}, {-1, 0.0}, {0, 0.0}, {1, 0.0}, {2, 0.05}, {3, 0.1}, {4, 0.9}, {5, 0.9}, {6, 0.9}, {7, 0.9}, {8, 0.99}, {9, 0.99}, {10, 0.99}};
        const unordered_map<int, float> NTT_ONE = {
            {-6, 0.0}, {-5, 0.0}, {-4, 0.0}, {-3, 0.0}, {-2, 0.0}, {-1, 0.0}, {0, 0.05}, {1, 0.15}, {2, 0.4}, {3, 0.9}, {4, 0.9}, {5, 0.9}, {6, 0.99}, {7, 0.99}, {8, 0.99}, {9, 0.99}, {10, 0.99}};
        const unordered_map<int, float> NTT_TWO = {
            {-6, 0.0}, {-5, 0.0}, {-4, 0.0}, {-3, 0.0}, {-2, 0.01}, {-1, 0.1}, {0, 0.5}, {1, 0.9}, {2, 0.99}, {3, 0.99}, {4, 0.99}, {5, 0.99}, {6, 0.99}, {7, 0.99}, {8, 0.99}, {9, 0.99}, {10, 0.99}};
    };
    /*-------------------------------------------
     TODO:
    --------------------------------------------*/
    inline static float probability_equation(spectra *spc, float pos_prior, float neg_prior){
        float probability = 0.0, numerator = 0.0, denominator = 0.0;
        
        // numerator = spc->d_pos * spc->ntt_pos * pos_prior;
        // denominator = (spc->d_pos * spc->ntt_pos * pos_prior) + (spc->d_neg * spc->ntt_neg * neg_prior);

        numerator = spc->d_pos * spc->ntt_pos * spc->nmc_pos * spc->m_pos * pos_prior;
        denominator = (spc->d_pos * spc->ntt_pos * spc->nmc_pos * spc->m_pos * pos_prior) + (spc->d_neg * spc->ntt_neg * spc->nmc_neg * spc->m_neg * neg_prior);
        
        probability = numerator / denominator;
        
        assert(!isnan(probability));
        
        // Adjusting for low values of pbty
        (probability < 0.001) ? probability = 0.001 : 0;
        
        return(probability);
    }
    
    /*---------------------------------------------
     TODO:
    -----------------------------------------------*/
    inline static float final_p_equation(spectra *spc, float pos_prior, float neg_prior){
        float final_pbty = 0.0;
        assert(!isnan(spc->d_pos) && !isnan(spc->d_neg));
        assert(!isnan(spc->ntt_pos) && !isnan(spc->ntt_neg));
        assert(!isnan(spc->nmc_pos) && !isnan(spc->nmc_neg));
        assert(!isnan(spc->m_pos) && !isnan(spc->m_neg));
        
        float numerator = spc->d_pos * spc->ntt_pos * spc->nmc_pos * spc->m_pos * pos_prior;
        float denom = (spc->d_pos * spc->ntt_pos * spc->nmc_pos * spc->m_pos * pos_prior) + (spc->d_neg * spc->ntt_neg * spc->nmc_neg * spc->m_neg * neg_prior);
        

        assert(!isnan(numerator) && !isnan(denom));
        final_pbty = numerator / denom;
        assert(!isnan(final_pbty));
        return(final_pbty);
    }
    
    /*----------------------------------------------
     TODO:
    -----------------------------------------------*/
    inline static float derive_dpos(spectra *spc, probability_table* pt){
        float d_pos = 0.0;
        float score = round(spc->discriminant_score);
        
        (score > 10) ? score = 10 : 0;
        (score < -6) ? score = -6 : 0;
        
        if(spc->num_termini == 2){
            d_pos = pt->NTT_TWO.at(score);
        }else if(spc->num_termini == 1){
            d_pos = pt->NTT_ONE.at(score);
        } else{
            d_pos = pt->NTT_ZERO.at(score);
        }
        
        return(d_pos);
    }
    /*-----------------------------------------------------
     TODO:
    -------------------------------------------------------*/
    inline static float derive_dneg(vector<spectra> *all_spectra){
        float d_neg = 0.0;
        for(auto &spc : (*all_spectra)){
            if(spc.num_termini == 0){
                ++d_neg;
            }
        }
        assert(!isnan(d_neg));
        d_neg = d_neg / (float)(*all_spectra).size();
        assert(!isnan(d_neg));
        return(d_neg);
    }

    /*----------------------------------------------------------
     TODO:
    ------------------------------------------------------------*/
    inline static float get_discrim_threshold(vector<spectra> *all_spectra){
        float thresh = 0.0, neg_mean = 0.0, num_neg = 0.0;
        float num_ntt_zero = 0.0;
        
        for(auto &spc : (*all_spectra)){
            if(spc.num_termini == 0){
                neg_mean += spc.discriminant_score;
                ++num_neg;
                ++num_ntt_zero;
            }
        }
        assert(!isnan(neg_mean));
        assert(!isnan(num_neg));
        
        // Nan value precautions (Generally only happens when database is constrained to consider
        // only fully tryptic peptides)
        (neg_mean == 0 || num_neg == 0) ? thresh = 0 : thresh = neg_mean / num_neg;
        
        float mean = 0.0;
        if(num_ntt_zero == 0){
            for(auto &spc : (*all_spectra)){
                mean += spc.discriminant_score;
            }
            mean = mean / (*all_spectra).size();
            thresh = mean;
        }
        
        assert(!isnan(thresh));
        
        return(thresh);
    }

    /*-------------------------------------------------------
     
     TODO:
    --------------------------------------------------------*/
    inline static float check_mixmodel_convergence(float prev_avg, vector<std::pair<float, float>>* curr_distribution_values){
        
        float curr_avg = 0.0;
        for(auto &d : (*curr_distribution_values)){
            curr_avg += d.first;
            curr_avg += d.second;
        }
        curr_avg = curr_avg / ((float)(*curr_distribution_values).size() * 2.0);
        
        if(abs(curr_avg - prev_avg) < 0.01){
            return(0.0);
        }else{
            return(curr_avg);
        }
    }

    

}

/////////////////////////////////////////////////////////////////////////////////////////////////
namespace PeptideHeretic {
    class HereticWarning{
        public:
            std::string message;
            HereticWarning(std::string msg):
                message(msg)
                {this->print_msg();}
        private:
            void print_msg(){
                std::cout << "[HERETIC WARNING]: " << this->message << std::endl;
            }
    };
    

}  // namespace PeptideHeretic



/*
  
   ___           __  _    __    __ __            __  _    
  / _ \___ ___  / /_(_)__/ /__ / // /__ _______ / /_(_)___
 / ___/ -_) _ \/ __/ / _  / -_) _  / -_) __/ -_) __/ / __/
/_/   \__/ .__/\__/_/\_,_/\__/_//_/\__/_/  \__/\__/_/\__/ 
        /_/                                               
 
*/

#ifndef DISCRIMINANT_FUNCTIONS_HPP
#define DISCRIMINANT_FUNCTIONS_HPP

#pragma once

#include <cmath>
#include <cassert>

class Comet_Coeffs {
    public:
        static float constant_coeff(int charge){
            float constant_coeffs[] = {0.646, -0.959, -1.460, -0.774, -0.598, -0.598, -0.598};
            return(constant_coeffs[charge]);
        }
        static float xcorr_coeff(int charge){
            float xcorr_coeffs[] = {5.49, 8.362, 9.933, 1.465, 3.89, 3.89, 3.89};
            return(xcorr_coeffs[charge]);
        }
        static float delta_coeff(int charge){
            float delta_coeffs[] = {4.643, 7.386, 11.149, 8.704, 7.271, 7.271, 7.271};
            return(delta_coeffs[charge]);
        }
        static float sprank_coeff(int charge){
            float sprank_coeffs[] = {-0.455, -0.194, -0.201, -0.331, -0.377, -0.377, -0.377};
            return(sprank_coeffs[charge]);
        }
        static float massd_coeff(int charge){
            float massd_coeffs[] = {-0.84, -0.314, -0.277, -0.277, -0.84, -0.84, -0.84};
            return(massd_coeffs[charge]);
        }
        static int max_lengths(int charge){
            float lengths[] = {100, 15, 25, 50, 100, 100, 100};
            return(lengths[charge]);
        }
        static int num_frags(int charge){
            float frags[] = {2, 2, 3, 4, 6, 6, 6};
            return(frags[charge]);
        }
        static float xcorr_prime(int charge, float xcorr_score, int sequence_length){
            float xcorr_prime = 0.0;

            int max_length = Comet_Coeffs::max_lengths(charge);
            int num_frags = Comet_Coeffs::num_frags(charge);

            if(sequence_length > max_length){
                sequence_length = max_length;
            }
            
            if(isnan(log(xcorr_score)) || isinf(log(xcorr_score))){
                xcorr_prime = 0.0;
            }
            else{
                xcorr_prime = (log(xcorr_score)) / (log(sequence_length * num_frags));
            }

            assert(!isnan(xcorr_prime));
            assert(!isinf(xcorr_prime));
            return(xcorr_prime);
        }
};

class Sequest_Coeffs{
    public:
        static float constant_coeff(int charge){
            float constant_coeffs[] = {0.646, -0.959, -1.460, -0.774, -0.598, -0.598, -0.598};
            return(constant_coeffs[charge]);
        }
        static float xcorr_coeff(int charge){
            float xcorr_coeffs[] = {5.49, 8.362, 9.933, 1.465, 3.89, 3.89, 3.89};
            return(xcorr_coeffs[charge]);
        }
        static float delta_coeff(int charge){
            float delta_coeffs[] = {4.643, 7.386, 11.149, 8.704, 7.271, 7.271, 7.271};
            return(delta_coeffs[charge]);
        }
        static float sprank_coeff(int charge){
            float sprank_coeffs[] = {-0.455, -0.194, -0.201, -0.331, -0.377, -0.377, -0.377};
            return(sprank_coeffs[charge]);
        }
        static float massd_coeff(int charge){
            float massd_coeffs[] = {-0.84, -0.314, -0.277, -0.277, -0.84, -0.84, -0.84};
            return(massd_coeffs[charge]);
        }
        static int max_lengths(int charge){
            float lengths[] = {100, 15, 25, 50, 100, 100, 100};
            return(lengths[charge]);
        }
        static int num_frags(int charge){
            float frags[] = {2, 2, 3, 4, 6, 6, 6};
            return(frags[charge]);
        }
        static float xcorr_prime(int charge, float xcorr_score, int sequence_length){
            float xcorr_prime = 0.0;

            int max_length = Sequest_Coeffs::max_lengths(charge);
            int num_frags = Sequest_Coeffs::num_frags(charge);

            if(sequence_length > max_length){
                sequence_length = max_length;
            }
            
            if(isnan(log(xcorr_score)) || isinf(log(xcorr_score))){
                xcorr_prime = 0.0;
            }
            else{
                xcorr_prime = (log(xcorr_score)) / (log(sequence_length * num_frags));
            }

            assert(!isnan(xcorr_prime));
            assert(!isinf(xcorr_prime));
            return(xcorr_prime);
        }
};

class Tandem_Coeffs {
    public:
        static float constant_coeff(int charge){
            float constant_coeffs[] = {0.72, 0.4, 0.2, 0.2, 0.2};
            return(constant_coeffs[charge]);
        }
        static float expect_coeff(int charge){
            float expect_coeffs[] = {0.43, 0.4, 0.5, 0.5, 0.5};
            return(expect_coeffs[charge]);
        }  
        static float delta_coeff(int charge){
            float delta_coeffs[] = {4.3, 4.0, 5.0, 5.0, 5.0};
            return(delta_coeffs[charge]);
        }
        static float length_coeff(int charge){
            float length_coeffs[] = {0.25, 0.25, 0.25, 0.25, 0.25};
            return(length_coeffs[charge]);
        }
};

class Morpheus_Coeffs {
    public:
        static float constant_coeff(int charge){
            return(-6);
        }
        static float score_coeff(int charge){
            return(15./38.);
        }
        static float length_coeff(int charge){
            return(0.0);
        }
};

class Myrimatch_Coeffs {
    public:
        static float constant_coeff(int charge){
            return(-4.0);
        }
        static float mvh_coeff(int charge){
            return(14./70.);
        }
        static float min_val(int charge){
            return(-4);
        }
        static float length_coeff(int charge){
            return(0.0);
        }


};

namespace Discriminant_Functions{
    static float tandem(int charge, float hyper_score, float expect_score, float next_score, float sequence_length){
        float d_score = 0.0;

        // Retain respective coefficients per charge parameter
        float constant_coeff = Tandem_Coeffs::constant_coeff(charge);
        float score_coeff = 0.0;
        float expect_coeff = Tandem_Coeffs::expect_coeff(charge);
        float delta_coeff = Tandem_Coeffs::delta_coeff(charge);
        float length_coeff = Tandem_Coeffs::length_coeff(charge);

        // Discriminant function
        d_score = (score_coeff * log(hyper_score)) + (expect_coeff * (0 - log(expect_score)))
            + (delta_coeff * (1.0 - (next_score / hyper_score)));
        
        // if (length_coeff){ d_score += length_coeff * sqrt(sequence_length); }

        d_score += constant_coeff;

        assert(!isnan(d_score));
        assert(!isinf(d_score));
        return(d_score);
    };


    static float comet(int charge, float xcorr_score, float delta_score, float sprank_score, float massd, int sequence_length){
        float d_score = 0.0;

        float constant_coeff = Comet_Coeffs::constant_coeff(charge);
        float xcorr_coeff = Comet_Coeffs::xcorr_coeff(charge);
        float delta_coeff = Comet_Coeffs::delta_coeff(charge);
        float sprank_coeff = Comet_Coeffs::sprank_coeff(charge);
        float massd_coeff = Comet_Coeffs::massd_coeff(charge);
        assert(!isnan(d_score));
        assert(!isinf(d_score));

        d_score = constant_coeff;
        d_score += xcorr_coeff * Comet_Coeffs::xcorr_prime(charge, xcorr_score, sequence_length);
        assert(!isnan(d_score));
        assert(!isinf(d_score));
        if(delta_score < 1.0){
            d_score += delta_coeff * delta_score;
        }
        assert(!isnan(d_score));
        assert(!isinf(d_score));

        
        if(!isnan(log(sprank_score)) && !isinf(log(sprank_score))){
            d_score += sprank_coeff * log(sprank_score);
        }
        d_score += massd_coeff * abs(massd);

        assert(!isnan(d_score));
        assert(!isinf(d_score));

        return(d_score);
    };

    static float sequest(int charge, float xcorr_score, float delta_score, float sprank_score, float massd, int sequence_length){
        float d_score = 0.0;

        float constant_coeff = Sequest_Coeffs::constant_coeff(charge);
        float xcorr_coeff = Sequest_Coeffs::xcorr_coeff(charge);
        float delta_coeff = Sequest_Coeffs::delta_coeff(charge);
        float sprank_coeff = Sequest_Coeffs::sprank_coeff(charge);
        float massd_coeff = Sequest_Coeffs::massd_coeff(charge);

        d_score += constant_coeff;
        d_score += xcorr_coeff * Sequest_Coeffs::xcorr_prime(charge, xcorr_score, sequence_length);

        if(delta_score < 1.0){ d_score += delta_coeff * delta_score; }
        if(sprank_score > 0){
            if(!isnan(log(sprank_score)) && !isinf(log(sprank_score))){
                d_score += sprank_coeff * log(sprank_score);
            }
        }
        else{
            d_score = (12 / 3.6) * xcorr_score - 1.11;
        }

        d_score += massd_coeff * abs(massd);

        assert(!isnan(d_score));
        assert(!isinf(d_score));

        return(d_score);
    };

    static float morpheus(int charge, float morpheus_score, int sequence_length){
        float d_score = 0.0;

        float constant_coeff = Morpheus_Coeffs::constant_coeff(charge);
        float score_coeff = Morpheus_Coeffs::score_coeff(charge);
        float length_coeff = Morpheus_Coeffs::length_coeff(charge);

        d_score += constant_coeff;
        d_score += (score_coeff * morpheus_score);

        if(length_coeff){ d_score += length_coeff * sequence_length; }

        assert(!isnan(d_score));
        assert(!isinf(d_score));
        return(d_score);
    };

    static float myrimatch(int charge){
        float d_score = 0.0;
        return(d_score);
    };

}

#endif
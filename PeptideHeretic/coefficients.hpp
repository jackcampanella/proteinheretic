/*
  
   ___           __  _    __    __ __            __  _    
  / _ \___ ___  / /_(_)__/ /__ / // /__ _______ / /_(_)___
 / ___/ -_) _ \/ __/ / _  / -_) _  / -_) __/ -_) __/ / __/
/_/   \__/ .__/\__/_/\_,_/\__/_//_/\__/_/  \__/\__/_/\__/ 
        /_/                                               
 
*/

#ifndef COEFFS_HPP
#define COEFFS_HPP

#pragma once

#include <unordered_map>
#include <vector>

using std::string;
using std::unordered_map;
using std::vector;

// Coefficients for single charged ions
namespace single_charged_ions {
//         const unordered_map<string, float> tandem = {
//             {"constant", 1.344},
//             {"hyperscore", 0.237},
//             {"nextscore", -0.160},
//             {"bscore", 0.04389},
//             {"yscore", -0.0293},
// //            {"cscore", 0.0},
// //            {"zscore", 0.0},
// //            {"ascore", 0.0},
// //            {"xscore", 0.0},
//             {"expect", -11.0373}, 
//             {"mass_diff", -0.84}};
        const unordered_map<string, float> tandem = {
            {"constant", 0.72},
            {"expect", 0.43},
            {"delta", 4.3},
            {"length", 0.25},
            {"min", -1.0}};

        // const unordered_map<string, float> sequest = {
        //     {"constant", 0.646},
        //     {"xcorr", 5.49},
        //     {"deltacn", 4.643},
        //     {"deltacnstar", 0.0},
        //     {"spscore", 0.0},
        //     {"sprank", -0.455},
        //     {"mass_diff", -0.84}};
        const unordered_map<string, float> sequest = {
            {"constant", -0.236},
            {"xcorr", 8.346},
            {"deltacn", 3.904},
            {"deltacnstar", 0.0},
            {"spscore", 0.0},
            {"sprank", -0.536},
            {"mass_diff", 0.0}};

        const unordered_map<string, float> comet = {
            {"constant", 0.646},
            {"xcorr", 5.49},
            {"deltacn", 4.643},
            {"deltacnstar", 0.0},
            {"spscore", 0.0},
            {"sprank", -0.455},
            {"mass_diff", -0.84}};

        // const unordered_map<string, float> myrimatch = {
        //     {"mvh", 0.89705422},
        //     {"mzFidelity",-0.42351636},
        //     {"xcorr", 0.56694346},
        //     {"mass_diff", 0.04884883}
        // };
        const unordered_map<string, float> myrimatch = {
            {"mvh", 0.88716779},
            {"mzFidelity",0.09649053},
            {"xcorr", 0.16384331},
            {"mass_diff", -0.02606981}
        };
        // const unordered_map<string, float> myrimatch = {
        //     {"mvh", 0.23552},
        //     {"mzFidelity", 0.0},
        //     {"xcorr", 1.955},
        //     {"mass_diff", -0.84}
        // };

    //    const unordered_map<string, float> morpheus = {
    //        {"Morpheus Score", -0.8896820},
    //        {"PSM q-value", -1.7798640},
    //        {"mass_diff", -0.4452863}
    //    };
        const unordered_map<string, float> morpheus = {
            {"Morpheus Score", 3.06076975},
            {"PSM q-value", 0.19879900},
            {"mass_diff", -0.01628509}
        };
        // const unordered_map<string, float> morpheus = {
        //     {"constant", 0.646},
        //     {"Morpheus Score", 1.25834},
        //     {"PSM q-value", 0.0},
        //     {"mass_diff", -0.84}
        // };



    // Add coefficient maps here for single charged ions

}  // namespace single_charged_ions

// Coefficients for double charged ions
namespace double_charged_ions {
//     const unordered_map<string, float> tandem = {
//         {"constant", 1.344},
//         {"hyperscore", 0.237},
//         {"nextscore", -0.160},
//         {"bscore", 0.04389},
//         {"yscore", -0.0293},
// //        {"cscore", 0.0},
// //        {"zscore", 0.0},
// //        {"ascore", 0.0},
// //        {"xscore", 0.0},
//         {"expect", -11.0373},
//         {"mass_diff", -0.314}};

   const unordered_map<string, float> tandem = {
            {"constant", 0.4},
            {"expect", 0.4},
            {"delta", 4.0},
            {"length", 0.25},
            {"min", -1.8}};

    // const unordered_map<string, float> sequest = {
    //     {"constant", -0.959},
    //     {"xcorr", 8.362},
    //     {"deltacn", 7.386},
    //     {"deltacnstar", 0.0},
    //     {"spscore", 0.0},
    //     {"sprank", -0.194},
    //     {"mass_diff", -0.314}};

    const unordered_map<string, float> sequest = {
        {"constant", -1.498},
        {"xcorr", 9.3},
        {"deltacn", 7.317},
        {"deltacnstar", 0.0},
        {"spscore", 0.0},
        {"sprank", -0.199},
        {"mass_diff", 0.0}};

    const unordered_map<string, float> comet = {
        {"constant", -0.959},
        {"xcorr", 8.362},
        {"deltacn", 7.386},
        {"deltacnstar", 0.0},
        {"spscore", 0.0},
        {"sprank", -0.194},
        {"mass_diff", -0.314}};

//    const unordered_map<string, float> myrimatch = {
//        {"mvh", 0.8569193},
//        {"mzFidelity", 0.0662274},
//        {"xcorr", 0.4597452},
//        {"mass_diff", 0.1138155}
//    };
    const unordered_map<string, float> myrimatch = {
        {"mvh", 0.7276373},
        {"mzFidelity", 0.5242667},
        {"xcorr", -0.2985851},
        {"mass_diff", -0.2985283}
    };
    // const unordered_map<string, float> myrimatch = {
    //     {"mvh", 0.39397},
    //     {"mzFidelity", 0.0},
    //     {"xcorr", 2.106},
    //     {"mass_diff", -0.314}
    // };

    // const unordered_map<string, float> morpheus = {
    //        {"Morpheus Score", -0.8896820},
    //        {"PSM q-value", -1.7798640},
    //        {"mass_diff", -0.4452863}
    // };
    const unordered_map<string, float> morpheus = {
           {"Morpheus Score", 3.06076975},
           {"PSM q-value", 0.19879900},
           {"mass_diff", -0.01628509}
    };
        // const unordered_map<string, float> morpheus = {
        //     {"constant", -0.959},
        //     {"Morpheus Score", 1.25834},
        //     {"PSM q-value", 0.0},
        //     {"mass_diff", -0.314}
        // };

    // Add coefficient maps here for double charged ions

}  // namespace double_charged_ions

// Coefficients for triple charged ions
namespace triple_charged_ions {
//     const unordered_map<string, float> tandem = {
//         {"constant", 2.274},
//         {"hyperscore", 0.255},
//         {"nextscore", -0.3225},
//         {"bscore", 0.0566},
//         {"yscore", 0.0787},
// //        {"cscore", 0.0},
// //        {"zscore", 0.0},
// //        {"ascore", 0.0},
// //        {"xscore", 0.0},
//         {"expect", -17.303},
//         {"mass_diff", -0.277}};

    const unordered_map<string, float> tandem = {
            {"constant", 0.2},
            {"expect", 0.5},
            {"delta", 5.0},
            {"length", 0.25},
            {"min", -1.1}};

    // const unordered_map<string, float> sequest = {
    //     {"constant", -1.460},
    //     {"xcorr", 9.933},
    //     {"deltacn", 11.149},
    //     {"deltacnstar", 0.0},
    //     {"spscore", 0.0},
    //     {"sprank", -0.201},
    //     {"mass_diff", -0.277}};

    const unordered_map<string, float> sequest = {
        {"constant", -1.975},
        {"xcorr", 10.685},
        {"deltacn", 11.263},
        {"deltacnstar", 0.0},
        {"spscore", 0.0},
        {"sprank", -0.207},
        {"mass_diff", 0.0}};

    const unordered_map<string, float> comet = {
        {"constant", -1.460},
        {"xcorr", 9.933},
        {"deltacn", 11.149},
        {"deltacnstar", 0.0},
        {"spscore", 0.0},
        {"sprank", -0.201},
        {"mass_diff", -0.277}};

    // const unordered_map<string, float> myrimatch = {
    //     {"mvh", 1.01443705},
    //     {"mzFidelity", -0.29874197},
    //     {"xcorr", 0.64542362},
    //     {"mass_diff", 0.05656488}
    // };
    const unordered_map<string, float> myrimatch = {
        {"mvh", 0.5310832},
        {"mzFidelity", 0.6614305},
        {"xcorr", -0.1780859},
        {"mass_diff", -0.2727402}
    };
    // const unordered_map<string, float> myrimatch = {
    //     {"mvh", 0.36374},
    //     {"mzFidelity", 0.0},
    //     {"xcorr", 1.95446},
    //     {"mass_diff", -0.277}
    // };

//    const unordered_map<string, float> morpheus = {
//        {"Morpheus Score", 0.44349},
//        {"PSM q-value", -1.96823},
//        {"mass_diff", 0.18025}
//    };
    const unordered_map<string, float> morpheus = {
        {"Morpheus Score", 2.84297240},
        {"PSM q-value", -0.17676432},
        {"mass_diff", 0.01353498}
    };
    // const unordered_map<string, float> morpheus = {
    //     {"constant", 1.0},
    //     {"Morpheus Score", -0.3614752},
    //     {"PSM q-value", -1.2009400},
    //     {"mass_diff", 0.3519280}
    //     };

    // Add coefficient maps here for triple charged ions

}  // namespace triple_charged_ions



#endif

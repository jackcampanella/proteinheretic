/*
    ___           __      _      __ __            __  _
   / _ \_______  / /____ (_)__  / // /__ _______ / /_(_)___
  / ___/ __/ _ \/ __/ -_) / _ \/ _  / -_) __/ -_) __/ / __/
 /_/  /_/  \___/\__/\__/_/_//_/_//_/\__/_/  \__/\__/_/\__/
                                                           
*/

#ifndef PROTEIN_HERETIC_HPP
#define PROTEIN_HERETIC_HPP
#pragma once

#include <iomanip>
#include <exception>

#include "PeptideHeretic/progress.hpp"
#include "PeptideHeretic/heretic.hpp"
#include "heretic_utils.hpp"

#include "PeptideHeretic/PepXMLParser/rapidxml-1.13/rapidxml.hpp"
#include "PeptideHeretic/PepXMLParser/rapidxml-1.13/rapidxml_print.hpp"

using std::ofstream;
using std::string;

using json = nlohmann::json;
using namespace rapidxml;

namespace ProteinHeretic {
    const int MAX_NSP = 10000;

    class Heretic {
       public:
        string input_pepxml, input_fasta;
        float initial_min_peptide_prob = 0.05;
        string sample_enzyme = "";
        string outfile_name = "";
        Graph<PeptideNode, ProteinNode> graph;
        vector<bin<float>> bins;
        Heretic(string &pepxml, string &fasta, bool output_pepxml = false, string outfile_name = "");
        void output_xml();
        void output_json();

       private:
        float prev_avg_pep_change = 1;
        float prev_avg_prot_change = 1;
        int mean_num_connected_peptides = 0;
        int min_num_connected_peptides = 10;
        int max_num_connected_peptides = 0;

        // Algorithm steps
        void init_estimate();
        void compute_weights();
        void compute_nsp_values();
        void compute_nsp_distributions();
        void build_bins();
        void update_peptides();
        void update_proteins();

        // Graph Data Structure
        void build_nodes(vector<spectra> &pepheretic_results);
        void build_edges();
        void train();
        void add_node(PeptideNode &node);
        void add_node(ProteinNode &node);
        int pep_node_exists(string &peptide_sequence);
        int prot_node_exists(string &protein_name);				

        // Helper functions
        bool check_convergence();
        void refresh_nsp();
        void refresh_bins();
        void clean();
        std::pair<float, float> get_priors();
        vector<int> get_connected_peptides(ProteinNode *node);
        vector<int> get_connected_proteins(PeptideNode *node);
        vector<int> get_sibling_peptides(PeptideNode *pep_node, ProteinNode *prot_node);
        void compute_confidence();
        string format_outfile_name(string &outfile_name);
    };


#endif

    /*-----------------------------------------------------------------------------------
     Protein Heretic's main constructor. Facilitates preprocessing with PeptideHeretic,
     building Heretic's graph data structure, and training algorithm's model
    ------------------------------------------------------------------------------------*/
    inline ProteinHeretic::Heretic::Heretic(string &pepxml_file, string &fasta_file, bool output_pepxml, string outfile_name) {
        HereticBanner();
        this->input_pepxml = pepxml_file;
        this->input_fasta = fasta_file;
        this->outfile_name = this->format_outfile_name(outfile_name);
        auto search_results = PepXml::parse(pepxml_file);
        auto search_method = PepXml::get_search_method(pepxml_file);
        this->sample_enzyme = PepXml::get_sample_enzyme(pepxml_file);

        PeptideHeretic::Heretic heretic(search_results, search_method, pepxml_file, fasta_file, output_pepxml);
        this->build_nodes(heretic.all_spectra);
        cout << " - Training Protein Model - " << this->graph.left_nodes.size() << " Peptides and " << this->graph.right_nodes.size() << " Proteins" << endl;
        this->build_edges();
        this->build_bins();
        this->init_estimate();
        this->train();
    }

    /*------------------------------------------------------------------------------------------------------
     Function: train()
     Facilitates the training phase of ProteinHeretic. Initially estimates of the weights are calculated,
     after which NSP values are computed and NSP distributions on those values are performed. Then, peptide
     and protein probabilities are adjusted. These steps are repeated until a fixed point is reached.
    --------------------------------------------------------------------------------------------------------*/
    inline void ProteinHeretic::Heretic::train() {
        int current_iteration = 1;
        int total_iterations = 1;
        const int MAX_ITERATIONS = 500;
        bool convergence_reached = false;

        // Shows model training progress
        progresscpp::ProgressBar progressbar(MAX_ITERATIONS - 1, 50);

        while (current_iteration < MAX_ITERATIONS) {
            // Computes weights of relationships
            this->compute_weights();

            // Computes nsp values and respective distributions
            this->compute_nsp_values();
            this->compute_nsp_distributions();

            // Updates peptide and protein probabilities
            this->update_peptides();
            this->update_proteins();

            // Checking convergence
            convergence_reached = this->check_convergence();
            total_iterations++;

            if (convergence_reached) {
                current_iteration = MAX_ITERATIONS;
                progressbar.complete();
            } else {
                current_iteration += 1;
                ++progressbar;
            }
            progressbar.display();
        }
        progressbar.done();
        cout << " (iterations to model: " << total_iterations << ")" << endl;

        // Post processing
        this->compute_confidence();
        // this->clean();
    }

    /*---------------------------------------------------------------------------------------
     Function: update_peptides()
     Updates each peptide's probability in accordance to NSP and Degen. Peptide Information.
     See equation 5 in documentation.
    ----------------------------------------------------------------------------------------*/
    inline void ProteinHeretic::Heretic::update_peptides() {
        // Retaining nsp distributions for each bin
        for (auto &bin : this->bins) {
            float nsp_pos = bin.pos_distribution;
            float nsp_neg = bin.neg_distribution;

            // Iterating through peptides in the bin
            for (auto &id : bin.member_ids) {
                auto *current_peptide = &this->graph.left_nodes[id];
                
                assert(current_peptide->nsp_values.size() == current_peptide->matched_proteins.size());
                
                // Constraining peptides with nsp value of 0 to have a pbty of 0
                if(current_peptide->nsp_values.size() == 1){
                    if(current_peptide->nsp_values[0] == 0.0){
                        current_peptide->prev_probability = 0.0;
                        current_peptide->probability = 0.0;
                    }
                    else{
                        float numerator = current_peptide->probability * nsp_pos;
                        float denominator = (current_peptide->probability * nsp_pos) + ((1.0 - current_peptide->probability) * nsp_neg);

                        // store the actual probability and updating previous probability
                        current_peptide->prev_probability = current_peptide->probability;
                        current_peptide->probability = (numerator / denominator);
                    }
                }
                else{
                    float numerator = current_peptide->probability * nsp_pos;
                    float denominator = (current_peptide->probability * nsp_pos) + ((1.0 - current_peptide->probability) * nsp_neg);

                    // store the actual probability and updating previous probability
                    current_peptide->prev_probability = current_peptide->probability;
                    current_peptide->probability = (numerator / denominator);
                }
            }
        }
    }

    /*--------------------------------------------------------------------------------------
     Function: update_proteins() - See equation 12 in documentation
     Updates each protein's probability in accordance to the adjusted peptide probabilities
    ---------------------------------------------------------------------------------------*/
    inline void ProteinHeretic::Heretic::update_proteins() {
        for(auto &protein : this->graph.right_nodes){
            
            
            // Forcing proteins with too low of number of connected peptides
            // to have probability of 0
            if(this->mean_num_connected_peptides > 1 && protein.connected_peptides.size() < 4){
                protein.prev_probability = 0.0;
                protein.probability = 0.0;
            }
            else{
                float product = 1.0;

                for(auto const pep_index : protein.connected_peptides){
                    auto *current_peptide = &this->graph.left_nodes[pep_index];
                    
                    float p = current_peptide->probability;
                    float w = this->graph.edges.elements[current_peptide->index][protein.index];
                    
                    product = product * (1.0 - (w * p));
                }
                protein.prev_probability = protein.probability;
                protein.probability = 1.0 - product;
            }
        }
    }

    /*--------------------------------------------------------------------------------------------------------
     Function: compute_nsp_distribution() - See equation 6 in docs
     Computes the nsp distributions for each nsp bin in our model. The probability that a correctly
     assigned peptide has NSP value 'x' in bin 'k' can be computed by summation over those peptides in bin 'k'
    ----------------------------------------------------------------------------------------------------------*/
    inline void ProteinHeretic::Heretic::compute_nsp_distributions() {
        float pos_prior = 0, neg_prior = 0;
        auto p = this->get_priors();
        pos_prior = p.first;
        neg_prior = p.second;

        // Refreshing the nsp bins
        this->refresh_bins();
        
        // Iterating through each peptide node and for each nsp value associated with
        // the peptide, we add it to the respective nsp bin
        for(auto const &pep_node : this->graph.left_nodes){
            for(auto const &val : pep_node.nsp_values){
                for(auto &bin : this->bins){
                    if(val >= bin.min && val < bin.max){
                        bin.member_ids.push_back(pep_node.index);
                    }
                }
            }
        }

        // NSP Distributions among correct and incorrect peptide assignments are
        // computed for each bin 'k'
        for (auto &bin : this->bins) {
            float nsp_pos = 0.0, nsp_neg = 0.0;

            // Summing over likelihoods of peptides in this bin
            if (bin.member_ids.size() != 0) {
                for (auto &id : bin.member_ids) {
                    auto *curr_peptide = &this->graph.left_nodes[id];
                    nsp_pos += curr_peptide->probability;
                    nsp_neg += (1.0 - curr_peptide->probability);
                }

                bin.pos_distribution = nsp_pos / ((float)bin.member_ids.size() * pos_prior);
                bin.neg_distribution = nsp_neg / ((float)bin.member_ids.size() * neg_prior);
            }
        }
    }

    /*----------------------------------------------------------------------------------------------------------
     Function: compute_nsp_values() - See equation 10
     Computes the nsp values for each peptide. NSP indicates the number of sibling peptides with their
     apportioned probabilities. A peptide is considered another's sibling if it's connected to the same protein.
    ------------------------------------------------------------------------------------------------------------*/
    inline void ProteinHeretic::Heretic::compute_nsp_values() {
        this->refresh_nsp();
        
        // Iterating through each protein in the graph
        for(auto const &protein : this->graph.right_nodes){
            float total_nsp = 0.0;
            
            // Retrieving weight and pbty of each connected peptide of this protein
            for(auto const index : protein.connected_peptides){
                auto *peptide = &this->graph.left_nodes[index];
                
                float p = peptide->probability;
                float w = this->graph.edges.elements[peptide->index][protein.index];
                total_nsp += (w * p);
            }
            
            // For each peptide we subtract it's contribution from the total nsp
            // to retrieve it's actual nsp value. Then we store it
            for(auto const index : protein.connected_peptides){
                float nsp = 0.0, p = 0.0, w = 0.0;
                auto *peptide = &this->graph.left_nodes[index];
                
                p = peptide->probability;
                w = this->graph.edges.elements[peptide->index][protein.index];
                
                // Subtracting contribution and storing in peptide node
                nsp = total_nsp - (w * p);
                
                peptide->nsp_values.push_back(nsp);
            }
        }
    }

    /*---------------------------------------------------------------------------------------------------------
     Function: compute_weights - See equation 8
     If peptide 'i' corresponds to N different proteins, then the relative weight, w, that peptide i actually
     correspnds to protein j, is determined according to the probability of protein j relative to
     all of those N different proteins. This function computes that weight.
    -----------------------------------------------------------------------------------------------------------*/
    inline void ProteinHeretic::Heretic::compute_weights() {
        for (int i = 0; i < this->graph.edges.rows; ++i) {
            for (int j = 0; j < this->graph.edges.columns; ++j) {
                if (this->graph.edges.elements[i][j] != 0) {
                    // A peptide will not contribute to any other proteins if it is not degenerate.
                    if (this->graph.left_nodes[i].degenerate) {
                        float updated_weight = 0;
                        float sum_other_protein_probs = 0;
                        // Maintaining a pointer to the peptide in question
                        auto *curr_pep = &this->graph.left_nodes[i];

                        // Maintaining a pointer to the protein in question
                        auto *curr_prot = &this->graph.right_nodes[j];

                        // We also need the other proteins connected to this peptide
                        auto other_proteins = this->get_connected_proteins(curr_pep);

                        // Then we compute the weight based on equation 8
                        for (int n = 0; n < other_proteins.size(); n++) {
                            auto prot_index = other_proteins[n];
                            sum_other_protein_probs += this->graph.right_nodes[prot_index].probability;
                        }

                        // Accounting for 0 values (to avoid undefined behavior)
                        if (curr_prot->probability <= 0 || sum_other_protein_probs <= 0) {
                            updated_weight = 0.0;
                        } else {
                            // Diving current protein's probability by the sum of the other proteins' probabilities
                            updated_weight = curr_prot->probability / sum_other_protein_probs;
                        }

                        // Storing the weight in the edge matrix
                        this->graph.edges.elements[i][j] = updated_weight;
                    }
                }
            }
        }
    }
    /*------------------------------------------------------
     Function: init_estimate()
     TODO:
    -------------------------------------------------------*/
    inline void ProteinHeretic::Heretic::init_estimate() {

        for(auto &protein : this->graph.right_nodes){
            // Estimating probabilty
            if(protein.connected_peptides.size() < 2){
                protein.probability = 0.001;
            }
            else{
                float avg_pep_pbty = 0.0;
                for(auto pep_index : protein.connected_peptides){
                    avg_pep_pbty += this->graph.left_nodes[pep_index].probability;
                }
                avg_pep_pbty = avg_pep_pbty / protein.connected_peptides.size();
                protein.probability = avg_pep_pbty;
            }

            // Checking min/max and avg num connected peptides
            this->mean_num_connected_peptides += protein.num_matched_peptides;
            protein.num_matched_peptides > this->max_num_connected_peptides ? this->max_num_connected_peptides = protein.num_matched_peptides : 0;
            protein.num_matched_peptides < this->min_num_connected_peptides ? this->min_num_connected_peptides = protein.num_matched_peptides : 0;
        }

        this->mean_num_connected_peptides /= this->graph.right_nodes.size();
        cout << "min num connected peps: " << this->min_num_connected_peptides << endl;
        cout << "Max num connected peps: " << this->max_num_connected_peptides << endl;
        cout << "Mean num connected peps: " << this->mean_num_connected_peptides << endl;
    }

    /*------------------------------------------------------------------------------------------------------
     Function: build_bins()
     NSP distributions are made discrete by 'binning.' This function handle the construction of those bins.
     Peptides of higher nsp value generally have a higher likelihood of being in the sample.
    --------------------------------------------------------------------------------------------------------*/
    inline void ProteinHeretic::Heretic::build_bins() {
        vector<double> nsp_ranges = {0, 0.1, 0.25, 0.5, 1.0, 2.0, 5.0, 15.0};
        for (int i = 0; i < 8; ++i) {
            float min = nsp_ranges[i], max = nsp_ranges[i + 1];

            if (i == 7) {
                max = MAX_NSP;
            }
            bin<float> new_bin(i, (float)min, (float)max);
            this->bins.push_back(new_bin);
        }
    }

    /*----------------------------------------------------------------------------------
     Function: build_nodes()
     Builds the set of peptide (left) and protein (right) nodes of our Bipartite graph
     data structure. If we encounter identical peptide sequences matched to the same
     protein, we consider these redundant and retain the max probability between the two.
     Further, this function ensures that we aren't including redundant proteins.
    ------------------------------------------------------------------------------------*/
    inline void ProteinHeretic::Heretic::build_nodes(vector<spectra> &pepheretic_results) {
        
        for (int i = 0; i < pepheretic_results.size(); ++i) {
            // Pointer to the peptide holding the information we are retaining
            auto *peptide = &pepheretic_results[i];
            
            // We don't process peptides with too low of a probability
            if(peptide->probability > 0.05){
                // First we check if this peptide already exists as a node
                int pep_index = pep_node_exists(peptide->sequence);
                if (pep_index != -1) {
                    // If pep already exists, then we should see if it's mapped to the same protein as
                    // the existing entry
                    auto *existing_peptide = &this->graph.left_nodes[pep_index];
                    if (peptide->sequence == existing_peptide->sequence) {
                        // If the peptide is mapped to the same protein, then we just retain
                        // the max pbty of the two
                        if (peptide->probability > existing_peptide->probability) {
                            existing_peptide->probability = peptide->probability;
                        }
                        // We also increment the number of instances of this peptide
                        existing_peptide->num_instances++;
                    }
                } 
                else {
                    int peptide_index = (int)this->graph.left_nodes.size();
                    
                    // If the peptide doesn't exist yet, then we just add it as a node to our graph
                    // Creating peptide node
                    PeptideNode pep_node(peptide_index, peptide->sequence, peptide->probability, peptide->num_matched_proteins);
                    
                    pep_node.prev_probability = pep_node.probability;
                    pep_node.init_probability = pep_node.probability;
                    pep_node.charge = peptide->charge;
                    pep_node.num_instances = 1;
                    pep_node.fval = peptide->discriminant_score;
                    pep_node.calc_neutral_pep_mass = peptide->calc_neutral_pep_mass;

                    // If the peptide is degenerate we must consider the alternative proteins
                    // it might be matched with
                    if (pep_node.num_matched_proteins > 1) {
                        pep_node.degenerate = true;
                        pep_node.matched_proteins = peptide->alternative_proteins;
                        pep_node.matched_proteins.push_back(peptide->matched_protein);
                    } else {
                        // Otherwise we are only concerned with the main match
                        pep_node.degenerate = false;
                        pep_node.matched_proteins.push_back(peptide->matched_protein);
                    }

                    // Checking for peptide modification info
                    if(peptide->has_mod_info){
                        pep_node.has_mod_info = true;
                        pep_node.modified_peptide = peptide->modified_peptide;
                        pep_node.mod_info = peptide->mod_info;

                        // Checking for nterm mod
                        if(peptide->has_nterm_mod){
                            pep_node.has_nterm_mod = true;
                            pep_node.nterm_mass = peptide->nterm_mass;
                        }
                        
                        // Checking for cterm mod
                        if(peptide->has_cterm_mod){
                            pep_node.has_cterm_mod = true;
                            pep_node.cterm_mass = peptide->cterm_mass;
                        }
                    }

                    // Storing peptide node in heretic's graph (left set of nodes)
                    // and incrementing id
                    this->add_node(pep_node);

                    // Handling protein(s) associated with this peptide
                    for (auto &protein : pep_node.matched_proteins) {
                        // Checking if it already exists
                        int index = prot_node_exists(protein);
                        if (index == -1) {
                            // If the node doesn't exist, then we add a new protein node
                            // the the graph's set of right nodes
                            int protein_index = (int)this->graph.right_nodes.size();
                            ProteinNode prot_node(protein_index, protein, 1);
                            prot_node.connected_peptides.push_back(pep_node.index);
                            prot_node.prev_probability = 1;
                            this->add_node(prot_node);

                        } else {
                            // If the protein already exists then we simply update
                            // the peptide information associated with this protein
                            this->graph.right_nodes[index].num_matched_peptides++;
                            this->graph.right_nodes[index].connected_peptides.push_back(pep_node.index);
                        }
                    }
                }
                
            }

            
        }
    }

    /*-------------------------------------------------------------------------------------------
     Function: build_edges()
     Builds the edge matrix which represents the relationships between the peptide and
     protein nodes. Each row corresponds to a given peptide's relationships whereas each column
     corresponds to each protein's relationships. A nonzero value at an index [i][j] indicates
     that peptide i corresponds to protein j.
    ---------------------------------------------------------------------------------------------*/
    inline void ProteinHeretic::Heretic::build_edges() {
        this->graph.edges = Matrix<float>(this->graph.left_nodes.size(), this->graph.right_nodes.size());

        // Iterating through peptide nodes
        for (int i = 0; i < this->graph.left_nodes.size(); ++i) {
            float contrib_score = 1;
            auto *current_node = &this->graph.left_nodes[i];

            // If the peptide is degenerate, we apportion it's weight with respect
            // to the number of other proteins it is possibly connected to
            if (current_node->degenerate) {
                contrib_score = (1.0 / (float)current_node->matched_proteins.size());
            }

            // Then we brute force our way to find which proteins this peptide is connected to
            // and update the matrix respectively
            for(auto const &protein : this->graph.right_nodes){
                for(auto const pep_index : protein.connected_peptides){
                    if(current_node->index == pep_index){
                        this->graph.edges.elements[i][protein.index] = contrib_score;
                    }
                }
            }
            
        }
    }

    /*-------------------------------------------------------------------------------------------
     Function: prot_node_exists()
     Checks for existence of node in our graph. If it does exist, it returns its location in the
     set of nodes of the graph. Otherwise it returns -1. Same logic applies to pep_node_exists()
    ---------------------------------------------------------------------------------------------*/
    inline int ProteinHeretic::Heretic::prot_node_exists(string &protein_name) {
        // If there aren't any protein nodes yet, then we don't look as to avoid
        // undefined behavior
        if (this->graph.right_nodes.size() < 1) {
            return (-1);
        }
        // Otherwise, we search for existence of the node
        else {
            auto iter = std::find_if(this->graph.right_nodes.begin(), this->graph.right_nodes.end(), [&protein_name](const auto &protein_node) { return protein_node.name == protein_name; });
            if (iter == this->graph.right_nodes.end()) {
                return (-1);
            } else {
                return (std::distance(this->graph.right_nodes.begin(), iter));
            }
        }
    }
    /*-------------------------------------------------------------------------------------------
     Function: pep_node_exists()
     TODO:
    ---------------------------------------------------------------------------------------------*/
    inline int ProteinHeretic::Heretic::pep_node_exists(string &peptide_sequence) {
        // If there aren't any peptide nodes yet, we don't search as to
        // avoid undefined behavior
        if (this->graph.left_nodes.size() < 1) {
            return (-1);
        }
        // Otherwise we search for the node
        else {
            auto iter = std::find_if(this->graph.left_nodes.begin(), this->graph.left_nodes.end(), [&peptide_sequence](const auto &peptide_node) { return peptide_node.sequence == peptide_sequence; });
            if (iter == this->graph.left_nodes.end()) {
                return (-1);
            } else {
                return (std::distance(this->graph.left_nodes.begin(), iter));
            }
        }
    }

    /*---------------------------------------------------------------------------------------------
        TODO: 
    ---------------------------------------------------------------------------------------------*/
    inline void ProteinHeretic::Heretic::compute_confidence(){
        float mean = this->mean_num_connected_peptides;
        float max = float(this->max_num_connected_peptides);
        float min = float(this->min_num_connected_peptides);

        float mid = (max - mean) / 2.0;

        for (auto &prot : this->graph.right_nodes){
            if(prot.num_matched_peptides >= 10){
                prot.confidence = 1.0;
            }
            else{
                prot.confidence = (float(prot.num_matched_peptides) - min) / (10 - min);
            }
        }
    }

    /*-------------------------------------------------------------------------------------------
     Function: get_priors()
     TODO:
    ---------------------------------------------------------------------------------------------*/
    inline std::pair<float,float> ProteinHeretic::Heretic::get_priors(){
        float pos_prior = 0.0;
        float neg_prior = 0.0;
        
        for(auto &pep_node : this->graph.left_nodes){
            pos_prior += pep_node.probability;
            neg_prior += (1.0 - pep_node.probability);
        }
        pos_prior = pos_prior / this->graph.left_nodes.size();
        neg_prior = neg_prior / this->graph.left_nodes.size();
        
        
        return(std::make_pair(pos_prior, neg_prior));
    }
    /*-------------------------------------------------------------------------------------------------------------
     Function: add_node()
     Adds a node to our graph (either left or right set) depending on the type of node supplied.
     Function override accomplishes this as overloads include compatibility for PeptideNode and ProteinNode types.
    ---------------------------------------------------------------------------------------------------------------*/
    inline void ProteinHeretic::Heretic::add_node(PeptideNode &node) {
        assert(node.index == this->graph.left_nodes.size());
        this->graph.left_nodes.push_back(node);
    }
    inline void ProteinHeretic::Heretic::add_node(ProteinNode &node) {
        assert(node.index == this->graph.right_nodes.size());
        this->graph.right_nodes.push_back(node);
    }

    /*------------------------------------------------------------------------------------------------------------
     Function: get_connected_proteins()
     Returns a set of protein indexes (corresponding to location in graph) associated with the provided peptide.
     We can optimally retrieve these indexes with a simple matrix look up and traversal.
    --------------------------------------------------------------------------------------------------------------*/
    inline vector<int> ProteinHeretic::Heretic::get_connected_proteins(PeptideNode *node) {
        vector<int> connected_protein_ids;
        auto pep_index = node->index;

        for (int j = 0; j < this->graph.edges.columns; j++) {
            //TODO: Need to have some error handling here to guard for safe access
            if (this->graph.edges.elements[pep_index][j]) {
                connected_protein_ids.push_back(j);
            }
        }
        return (connected_protein_ids);
    }

    /*------------------------------------------------------------------------------------------------------
     Function: refresh_nsp()
     Clears the nsp values in the nsp vector for each peptide node. As the model proceeds through
     the training phase, probabilities are subject to change which impacts the nsp values of each
     peptide. Calling this function ensures that our nsp distributions are considering up-to-date probabilities
    --------------------------------------------------------------------------------------------------------*/
    inline void ProteinHeretic::Heretic::refresh_nsp() {      
        for(auto &peptide : this->graph.left_nodes){
            peptide.nsp_values.clear();
        }
    }

    /*---------------------------------------------------------------------------------------------------
     Function: refresh_bins()
     Clears the members of our nsp bins. Since nsp values are subject to change during subsequent
     iterations, peptides are subject to be placed in different bins. Thus, calling this function ensures
     that our nsp distributions are not considering previous iteration data.
    -----------------------------------------------------------------------------------------------------*/
    inline void ProteinHeretic::Heretic::refresh_bins() {
        for (auto &bin : this->bins) {
            bin.member_ids.clear();
        }
    }

    inline string ProteinHeretic::Heretic::format_outfile_name(string &outfile_name){
        
        // returns file extension
        auto get_extension = [](string filename){

        };

        // returns file name in a path without extension
        auto get_file_name = [](string filepath){

        };
        
        
        string formatted_name = "heretic.prot.xml";
        // If there wasn't a filename provided, 
        // default to using modified input pepxml name 
        if(outfile_name.empty()){

        }
        else{

        }

        return(formatted_name);
    }

    /*---------------------------------------------------------------------------
     Function: clean()
     Removes the proteins that do not pass the probability threshold to achieve
     a subset of proteins that best explains the dataset.
    ----------------------------------------------------------------------------*/
    inline void ProteinHeretic::Heretic::clean() {
        float threshold = 0.05;
        this->graph.right_nodes.erase(
            std::remove_if(this->graph.right_nodes.begin(), this->graph.right_nodes.end(),
                           [&threshold](ProteinNode &protein) {
                               return protein.probability < threshold;
                           }),
            this->graph.right_nodes.end());
    }

    /*--------------------------------------------------------------------------------------------------
     Function: check_convergence
     ProteinHeretic's algorithm updates distributions and probabilities until convergence is reached.
     This function checks whether or not the model has reached convergence by determining the rate of
     change in probabilities of Proteins and Peptides.
    ----------------------------------------------------------------------------------------------------*/
    inline bool ProteinHeretic::Heretic::check_convergence() {
        bool convergence = false;
        float all_diff_pep_p = 0, all_diff_prot_p = 0;
        float avg_p_change = 0;

        // Summing over current change of peptide node probabilities
        for (auto &pep : this->graph.left_nodes) {
            all_diff_pep_p += abs(pep.probability - pep.prev_probability);
        }
        // Summing over current change of protein probabilities
        for (auto &prot : this->graph.right_nodes) {
            all_diff_prot_p += abs(prot.probability - prot.prev_probability);
        }

        // Computing average change in probability for peptides and proteins
        all_diff_pep_p /= this->graph.left_nodes.size();
        all_diff_prot_p /= this->graph.right_nodes.size();

        // Summing and apportioning average probabilities
        avg_p_change = (all_diff_pep_p + all_diff_prot_p) / 2;

        // If the average change in probability is small enough we
        // can consider convergence reached
        if (avg_p_change < 0.05) {
            convergence = true;
        }

        // Updating current rate of change
        this->prev_avg_pep_change = all_diff_pep_p;
        this->prev_avg_prot_change = all_diff_prot_p;

        return (convergence);
    }

    /*-----------------------------------------------
    
    -------------------------------------------------*/
    inline void ProteinHeretic::Heretic::output_xml(){
        // string output_file = "heretic.prot.xml";
        string output_file = this->outfile_name;
        rapidxml::xml_document<> doc;
        // Creating root node, protein summary
        xml_node<> *root_node = doc.allocate_node(node_element, "protein_summary");
        doc.append_node(root_node);
        xml_attribute<> *xmlns = doc.allocate_attribute("xmlns", "http://regis-web.systemsbiology.net/protXML");
        xml_attribute<> *xsi = doc.allocate_attribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        xml_attribute<> *schema_location = doc.allocate_attribute("xsi:schemaLocation", "http://sashimi.sourceforge.net/schema_revision/protXML/protXML_v6.xsd");
        xml_attribute<> *summary_xml = doc.allocate_attribute("summary_xml", output_file.c_str());
        root_node->append_attribute(xmlns);
        root_node->append_attribute(xsi);
        root_node->append_attribute(schema_location);
        root_node->append_attribute(summary_xml);
        
        // Creating protein summary header node, first child of protein summary
        xml_node<> *protein_summary_header = doc.allocate_node(node_element, "protein_summary_header");
        char *database = doc.allocate_string(this->input_fasta.c_str());
        char *source_files = doc.allocate_string(this->input_pepxml.c_str());
        char *init_min_pep_prob = doc.allocate_string(std::to_string(this->initial_min_peptide_prob).c_str());
        char *samp_enzyme = doc.allocate_string(this->sample_enzyme.c_str());

        xml_attribute<> *database_attr = doc.allocate_attribute("reference_database", database);
        xml_attribute<> *source_file_attr = doc.allocate_attribute("source_files", source_files);
        xml_attribute<> *init_min_prob_attr = doc.allocate_attribute("initial_min_peptide_prob", init_min_pep_prob);
        xml_attribute<> *sample_enzyme_attr = doc.allocate_attribute("sample_enzyme", samp_enzyme);


        protein_summary_header->append_attribute(database_attr);
        protein_summary_header->append_attribute(source_file_attr);
        protein_summary_header->append_attribute(init_min_prob_attr);
        protein_summary_header->append_attribute(sample_enzyme_attr);
        root_node->append_node(protein_summary_header);
        
        // Creating program details node appending to protein_summary
        xml_node<> *program_details = doc.allocate_node(node_element, "program_details");
        xml_attribute<> *analysis = doc.allocate_attribute("analysis", "proteinheretic");
        xml_attribute<> *version = doc.allocate_attribute("version", "C++ 14 (gcc compiler: 8.3.1 20191121 (Red Hat 8.3.1-5) clang version 11.0.3 (clang-1103.0.32.62)");
        program_details->append_attribute(analysis);
        program_details->append_attribute(version);
        protein_summary_header->append_node(program_details);
        
        // Creating proteinheretic details node appending to program details node
        xml_node<> *heretic_details = doc.allocate_node(node_element, "proteinheretic_details");
        xml_attribute<> *options = doc.allocate_attribute("options", "these are where optional parameters will go");
        heretic_details->append_attribute(options);
        program_details->append_node(heretic_details);
        
        // nsp info node 
        xml_node<> *nsp_information = doc.allocate_node(node_element, "nsp_information");
        heretic_details->append_node(nsp_information);

        // nsp distribution nodes
        int bin_number = 0;
        for(auto &bin : this->bins){
            char *bin_num = doc.allocate_string(std::to_string(bin_number).c_str());
            char *lower_bound = doc.allocate_string(std::to_string(bin.min).c_str());
            char *upper_bound = doc.allocate_string(std::to_string(bin.max).c_str());
            char *members = doc.allocate_string(std::to_string(bin.member_ids.size()).c_str());
            char *neg_freq = doc.allocate_string(std::to_string(bin.neg_distribution).c_str());
            char *pos_freq = doc.allocate_string(std::to_string(bin.pos_distribution).c_str());

            xml_attribute<> *bin_num_attr = doc.allocate_attribute("bin_no", bin_num);
            xml_attribute<> *lower_attr = doc.allocate_attribute("nsp_lower_bound", lower_bound);
            xml_attribute<> *upper_attr = doc.allocate_attribute("nsp_upper_bound", upper_bound);
            xml_attribute<> *members_attr = doc.allocate_attribute("num_spectra", members);
            xml_attribute<> *neg_freq_attr = doc.allocate_attribute("neg_freq", neg_freq);
            xml_attribute<> *pos_freq_attr = doc.allocate_attribute("pos_freq", pos_freq);

            xml_node<> *nsp_distr = doc.allocate_node(node_element, "nsp_distribution");
            nsp_distr->append_attribute(bin_num_attr);
            nsp_distr->append_attribute(lower_attr);
            nsp_distr->append_attribute(upper_attr);
            nsp_distr->append_attribute(members_attr);
            nsp_distr->append_attribute(neg_freq_attr);
            nsp_distr->append_attribute(pos_freq_attr);

            nsp_information->append_node(nsp_distr);
            ++bin_number;
        }

        // protein summary data filter nodes
        // for min_prob 0.00 - 0.90
        for(float min_probability = 0.00; min_probability <= 0.90; min_probability += 0.10){
            char *min_prob = doc.allocate_string(std::to_string(min_probability).c_str());
            
            xml_attribute<> *min_prob_attr = doc.allocate_attribute("min_probability", min_prob);
            xml_attribute<> *sens_attr = doc.allocate_attribute("sensitivity", "1.000");
            xml_attribute<> *false_positive_err_attr = doc.allocate_attribute("false_positive_error_rate", "0.000");
            xml_attribute<> *pred_corr_attr = doc.allocate_attribute("predicted_num_correct", "Val");
            xml_attribute<> *pred_incorr_attr = doc.allocate_attribute("predicted_num_incorrect", "Val");

            xml_node<> *protein_summary_data_filter = doc.allocate_node(node_element, "protein_summary_data_filter");
            protein_summary_data_filter->append_attribute(min_prob_attr);
            protein_summary_data_filter->append_attribute(sens_attr);
            protein_summary_data_filter->append_attribute(false_positive_err_attr);
            protein_summary_data_filter->append_attribute(pred_corr_attr);
            protein_summary_data_filter->append_attribute(pred_incorr_attr);
            
            heretic_details->append_node(protein_summary_data_filter);
        }

        // For min_prob 0.90 - 0.95
        float min_probability = 0.90;
        char *min_prob = doc.allocate_string(std::to_string(min_probability).c_str());

        xml_attribute<> *min_prob_attr = doc.allocate_attribute("min_probability", min_prob);
        xml_attribute<> *sens_attr = doc.allocate_attribute("sensitivity", "1.000");
        xml_attribute<> *false_positive_err_attr = doc.allocate_attribute("false_positive_error_rate", "0.000");
        xml_attribute<> *pred_corr_attr = doc.allocate_attribute("predicted_num_correct", "Val");
        xml_attribute<> *pred_incorr_attr = doc.allocate_attribute("predicted_num_incorrect", "Val");

        xml_node<> *protein_summary_data_filter = doc.allocate_node(node_element, "protein_summary_data_filter");
        protein_summary_data_filter->append_attribute(min_prob_attr);
        protein_summary_data_filter->append_attribute(sens_attr);
        protein_summary_data_filter->append_attribute(false_positive_err_attr);
        protein_summary_data_filter->append_attribute(pred_corr_attr);
        protein_summary_data_filter->append_attribute(pred_incorr_attr);
            
        heretic_details->append_node(protein_summary_data_filter);

        // for min_prob 0.95 - 1.00
        for(float min_probability = 0.95; min_probability <= 1.00; min_probability += 0.01){
            char *min_prob = doc.allocate_string(std::to_string(min_probability).c_str());

            xml_attribute<> *min_prob_attr = doc.allocate_attribute("min_probability", min_prob);
            xml_attribute<> *sens_attr = doc.allocate_attribute("sensitivity", "1.000");
            xml_attribute<> *false_positive_err_attr = doc.allocate_attribute("false_positive_error_rate", "0.000");
            xml_attribute<> *pred_corr_attr = doc.allocate_attribute("predicted_num_correct", "Val");
            xml_attribute<> *pred_incorr_attr = doc.allocate_attribute("predicted_num_incorrect", "Val");

            xml_node<> *protein_summary_data_filter = doc.allocate_node(node_element, "protein_summary_data_filter");
            protein_summary_data_filter->append_attribute(min_prob_attr);
            protein_summary_data_filter->append_attribute(sens_attr);
            protein_summary_data_filter->append_attribute(false_positive_err_attr);
            protein_summary_data_filter->append_attribute(pred_corr_attr);
            protein_summary_data_filter->append_attribute(pred_incorr_attr);
            
            heretic_details->append_node(protein_summary_data_filter);
        }

        // Adding protein groups to xml
        int group_number = 1;
        for(auto &prot : this->graph.right_nodes){
            // Have to allocate string for group num
            char *group_num = doc.allocate_string(std::to_string(group_number).c_str());
            
            xml_node<> *protein_group = doc.allocate_node(node_element, "protein_group");
            root_node->append_node(protein_group);
            xml_attribute<> *group_number_attr = doc.allocate_attribute("group_number", group_num);
            protein_group->append_attribute(group_number_attr);
            
        
            xml_node<> *protein = doc.allocate_node(node_element, "protein");
            xml_attribute<> *protein_name = doc.allocate_attribute("protein_name", prot.name.c_str());
            char *prot_pbty = doc.allocate_string(std::to_string(prot.probability).c_str());
            char *num_peptides = doc.allocate_string(std::to_string(prot.connected_peptides.size()).c_str()); 
            char *conf = doc.allocate_string(std::to_string(prot.confidence).c_str());

            xml_attribute<> *protein_pbty = doc.allocate_attribute("probability", prot_pbty);
            xml_attribute<> *num_peps_att = doc.allocate_attribute("total_number_peptides", num_peptides);
            xml_attribute<> *total_num_dist_pep_attr = doc.allocate_attribute("total_number_distinct_peptides", num_peptides);
            xml_attribute<> *conf_attr = doc.allocate_attribute("confidence", conf);

            protein->append_attribute(protein_name);
            protein->append_attribute(protein_pbty);
            protein->append_attribute(num_peps_att);
            protein->append_attribute(total_num_dist_pep_attr);
            protein->append_attribute(conf_attr);
            
            protein_group->append_node(protein);
            
            // Iterating through connected peptides
            for(auto &pep_index : prot.connected_peptides){
                auto pep = this->graph.left_nodes[pep_index];
                char *sequence = doc.allocate_string(pep.sequence.c_str());
                char *charge = doc.allocate_string(std::to_string(pep.charge).c_str());
                char *init_pbty = doc.allocate_string(std::to_string(pep.init_probability).c_str());
                char *pbty = doc.allocate_string(std::to_string(pep.probability).c_str());
                char *fval = doc.allocate_string(std::to_string(pep.fval).c_str());
                
                char *num_instances = doc.allocate_string(std::to_string(pep.num_instances).c_str());
                char *mass = doc.allocate_string(std::to_string(pep.calc_neutral_pep_mass).c_str());

                string is_non_degen = "";
                pep.degenerate ? is_non_degen = "N" : is_non_degen = "Y";
                char *degen = doc.allocate_string(is_non_degen.c_str());

                xml_node<> *peptide = doc.allocate_node(node_element, "peptide");
                xml_attribute<> *pep_sequence = doc.allocate_attribute("peptide_sequence", sequence);
                xml_attribute<> *charge_att = doc.allocate_attribute("charge", charge);
                xml_attribute<> *init_pbty_att = doc.allocate_attribute("initial_probability", init_pbty);
                xml_attribute<> *pep_pbty_att = doc.allocate_attribute("nsp_adjusted_probability", pbty);
                xml_attribute<> *fval_att = doc.allocate_attribute("fval", fval);
                xml_attribute<> *weight_attr = doc.allocate_attribute("weight", "val");
                xml_attribute<> *group_weight_attr = doc.allocate_attribute("group_weight", "val");
                xml_attribute<> *nondegen_ev_attr = doc.allocate_attribute("is_nondegenerate_evidence", degen);
                xml_attribute<> *n_term_attr = doc.allocate_attribute("n_enzymatic_termini", "val");
                xml_attribute<> *nsp_attr = doc.allocate_attribute("n_sibling_peptides", "val");
                xml_attribute<> *nsp_bin_attr = doc.allocate_attribute("n_sibling_peptides_bin", "val");
                xml_attribute<> *n_instances_attr = doc.allocate_attribute("n_instances", num_instances);
                xml_attribute<> *exp_tot_instances_attr = doc.allocate_attribute("exp_tot_instances", num_instances);
                xml_attribute<> *contr_ev_attr = doc.allocate_attribute("is_contributing_evidence", "Y");
                xml_attribute<> *calc_mass_attr = doc.allocate_attribute("calc_neutral_pep_mass", mass);

                peptide->append_attribute(pep_sequence);
                peptide->append_attribute(charge_att);
                peptide->append_attribute(init_pbty_att);
                peptide->append_attribute(pep_pbty_att);
                peptide->append_attribute(fval_att);
                peptide->append_attribute(weight_attr);
                peptide->append_attribute(group_weight_attr);
                peptide->append_attribute(nondegen_ev_attr);
                peptide->append_attribute(n_term_attr);
                peptide->append_attribute(nsp_attr);
                peptide->append_attribute(nsp_bin_attr);
                peptide->append_attribute(n_instances_attr);
                peptide->append_attribute(exp_tot_instances_attr);
                peptide->append_attribute(contr_ev_attr);
                peptide->append_attribute(calc_mass_attr);

                protein->append_node(peptide);

                // Modification info for peptides
                if(pep.has_mod_info){
                    xml_node<> *modification_info = doc.allocate_node(node_element, "modification_info");
                    
                    
                    // Appending modified_peptide attribute if it exists
                    if(!pep.modified_peptide.empty()){
                        char *mod_pep_sequence = doc.allocate_string(pep.modified_peptide.c_str());
                        xml_attribute<> *mod_pep_attr = doc.allocate_attribute("modified_peptide", mod_pep_sequence);
                        modification_info->append_attribute(mod_pep_attr);
                    }

                    // Appending mod_nterm_mass attribute if it exists
                    if(pep.has_nterm_mod){
                        char *nterm_mass = doc.allocate_string(std::to_string(pep.nterm_mass).c_str());
                        xml_attribute<> *nterm_mod = doc.allocate_attribute("mod_nterm_mass", nterm_mass);
                        modification_info->append_attribute(nterm_mod);
                    }

                    // Appending mod_cterm_mass attribute if it exists
                    if(pep.has_cterm_mod){
                        char *cterm_mass = doc.allocate_string(std::to_string(pep.cterm_mass).c_str());
                        xml_attribute<> *cterm_mod = doc.allocate_attribute("mod_cterm_mass", cterm_mass);
                        modification_info->append_attribute(cterm_mod);
                    }

                    peptide->append_node(modification_info);

                    // Iterating through all possible mods of the peptide
                    for(auto &mod_details : pep.mod_info){
                        char *position = doc.allocate_string(std::to_string(mod_details.position).c_str());
                        char *mass = doc.allocate_string(std::to_string(mod_details.mass).c_str());

                        xml_attribute<> *position_attr = doc.allocate_attribute("position", position);
                        xml_attribute<> *mass_attr = doc.allocate_attribute("mass", mass);

                        xml_node<> *mod_aminoacid_mass = doc.allocate_node(node_element, "mod_aminoacid_mass");
                        mod_aminoacid_mass->append_attribute(position_attr);
                        mod_aminoacid_mass->append_attribute(mass_attr);
                        modification_info->append_node(mod_aminoacid_mass);
                    }
                }
            }
            ++group_number;
        }
        ofstream outfile(output_file);
        outfile << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" << endl;
        outfile << doc;
        outfile.close();
    }

    /*-------------------------------------------------
    
    --------------------------------------------------*/
    inline void ProteinHeretic::Heretic::output_json(){
        string output_file = "proteinheretic.json";

        ofstream outfile(output_file);
        json results = {};
        cout << "Writing to file" << endl;
        for (auto &prot : this->graph.right_nodes) {
            json protein = {};
            json peptides = {};
    
            json prot_data = {
                {"protein_name", prot.name},
                {"probability", prot.probability},
                {"total_number_peptides", prot.connected_peptides.size()}};
            protein.push_back(prot_data);
    
            for (auto &pep_id : prot.connected_peptides) {
                auto pep = this->graph.left_nodes[pep_id];
                json j_pep = {
                    {"peptide_sequence", pep.sequence},
                    {"charge", pep.charge},
                    {"init_probability", pep.init_probability},
                    {"adjusted_probability", pep.probability},
                    {"nsp", pep.nsp_values},
                    {"fval", pep.fval},
                    {"degenerate", pep.degenerate}};
                peptides.push_back(j_pep);
            }
            protein.push_back(peptides);
            results.push_back(protein);
        }
        outfile << std::setw(4) << results << endl;
        outfile.close();
    }
}
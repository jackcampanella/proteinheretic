#include <ctime>
#include <fstream>
#include <iostream>

#include "heretic.hpp"
#include "include/cxxopts.hpp"

using std::cout;
using std::endl;
using std::ofstream;
using std::string;

cxxopts::Options configure_options(){
    cxxopts::Options options("./ProteinHeretic", "A Statistical Model for MS/MS Protein Identification (Inspired by ProteinProphet)");
    options.add_options()
    ("p,pepxml", "Search algorithm results containing list of spectra and scores", cxxopts::value<std::string>())
    ("f,fasta", "Protein Database used with Search Algorithm", cxxopts::value<std::string>())
    ("min_prob", "Set the minimum peptide probability threshold")
    ("pep_results", "Output peptide level verification results (stored in .pep.xml format)", cxxopts::value<bool>()->default_value("false"))
    ("o,outfile_name", "Specify the name of the resulting output prot.xml file", cxxopts::value<std::string>())
    ("h,help", "Print usage")
    ;
    return(options);
}

int main(int argc, char **argv) {
    //Starting a timer
    auto start = std::chrono::high_resolution_clock::now();
    srand(static_cast<unsigned int>(time(0)));
    
    // Configuring options
    auto ops = configure_options();
    auto parsed_ops = ops.parse(argc, argv);
    
    // Help message request
    if(parsed_ops.count("help")){
        cout << ops.help() << endl;
        exit(0);
    }

    string pepxml_file = "";
    string fasta_file = "";
    
    // Verifying pepxml provided
    if(parsed_ops.count("pepxml") == 0){
        HereticError("Pepxml file not provided.");
        exit(0);
    }
    else{
        pepxml_file = parsed_ops["pepxml"].as<std::string>();
    }
    // Verifying protein database provided
    if(parsed_ops.count("fasta") == 0){
        HereticError("Protein Database not provided.");
        exit(0);
    }
    else{
        fasta_file = parsed_ops["fasta"].as<std::string>();
    }

    // Performing ProteinHeretic
    try{
        ProteinHeretic::Heretic heretic(pepxml_file, fasta_file, parsed_ops["pep_results"].as<bool>());
        heretic.output_xml();
        cout << "Final set of Proteins: " << heretic.graph.right_nodes.size() << endl;
    }
    catch (...){
        HereticError();
        exit(0);
    }
    
    auto end = std::chrono::high_resolution_clock::now();
    cout << "Total execution time: " << std::chrono::duration<double>(end - start).count() << " seconds" << "\n" << endl;
    return (0);
}

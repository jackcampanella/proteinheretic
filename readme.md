# Protein Heretic
A statistical tool that identifies protein probabilities using peptides assigned to MS/MS spectra. 


## Usage

[Build]
```bash
    g++ -std=c++14 heretic.cpp -o ProteinHeretic
```

[Standard Usage]
```bash
./ProteinHeretic -p input/tandem/5b9c4f1e-db2a-4173-a6b1-fc928f4c584e_tandem.pep.xml -f input/18mix092106.fasta
```

[Show PeptideLevel results]
```bash
./ProteinHeretic -p input/tandem/5b9c4f1e-db2a-4173-a6b1-fc928f4c584e_tandem.pep.xml -f input/18mix092106.fasta --pep_results true
```